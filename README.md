#Sobre o Projeto:

Este projeto tem o objetivo de compartilhar as publicações científicas realizadas nos câmpus do IFC de Rio do Sul - SC. Mais adiante, poderá ser aprimorado.

As configurações do ambiente, assim como outras instruções de uso, estarão nas páginas da **Wiki**.