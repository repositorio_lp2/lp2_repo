package br.edu.ifc.riodosul.repositoriocientifico.helper;

public class ContactValidation extends GenericValidation  {
	private ContactForm contact;
	private boolean validContactData;
	private final int NAME_MIN_LENGTH = 3;
	private final int NAME_MAX_LENGTH = 45;
	private final int SUBJECT_MIN_LENGTH = 8;
	private final int SUBJECT_MAX_LENGTH = 140;
	private final int MESSAGE_CONTENT_MIN_LENGTH = 10;
	private final int MESSAGE_CONTENT_MAX_LENGTH = 255;
	private final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";;

	public ContactValidation() {
		this.setValidContactData(true);
	}
	
	public boolean validate() {
		validateName();
		validateEmail();
		validateSubject();
		validateMessageContent();
		return isValidContactData();
	}
	
	private void validateName() {
		if (isNullOrEmpty(contact.getName())) {
			addMessage("Nome - Campo Vazio");
			setValidContactData(false);
		}
		else if (!isStrLenEnough(contact.getName(),NAME_MIN_LENGTH, NAME_MAX_LENGTH)) {
			addMessage("Nome - inválido; Tamanho Mínimo: " + NAME_MIN_LENGTH + " caracteres, Tamanho Máximo: " + NAME_MAX_LENGTH + " caracteres.");
			setValidContactData(false);
		}
	}
	
	private void validateEmail() {
		if (isNullOrEmpty(contact.getEmail())) {
				addMessage("E-mail não pode ficar em branco.");
				setValidContactData(false);
		}
		else if (!contact.getEmail().matches(EMAIL_REGEX)) {
			addMessage("E-mail inválido.");
			setValidContactData(false);
		}
	}
	
	private void validateSubject() {
		if (isNullOrEmpty(contact.getSubject())) {
			addMessage("Assunto - Campo Vazio");
			setValidContactData(false);
		}
		else if (!isStrLenEnough(contact.getSubject(), SUBJECT_MIN_LENGTH, SUBJECT_MAX_LENGTH)) {
			addMessage("Assunto - inválido; Tamanho Mínimo: " + SUBJECT_MIN_LENGTH + " caracteres, Tamanho Máximo: " + SUBJECT_MIN_LENGTH + " caracteres.");
			setValidContactData(false);
		}
	}

	private void validateMessageContent() {
		if (isNullOrEmpty(contact.getMessageContent())) {
			addMessage("Mensagem - Campo Vazio");
			setValidContactData(false);
		}
		else if (!isStrLenEnough(contact.getMessageContent(), MESSAGE_CONTENT_MIN_LENGTH, MESSAGE_CONTENT_MAX_LENGTH)) {
			addMessage("Mensagem - inválida; Tamanho Mínimo: " + MESSAGE_CONTENT_MIN_LENGTH + " caracteres, Tamanho Máximo: " + MESSAGE_CONTENT_MAX_LENGTH + " caracteres.");
			setValidContactData(false);
		}
	}
	
	public void setValidContactData(boolean validContactData) {
		this.validContactData = validContactData;
	}
	
	public boolean isValidContactData() {
		return validContactData;
	}

	public ContactForm getContact() {
		return contact;
	}

	public void setContact(ContactForm contact) {
		this.contact = contact;
	}
}
