package br.edu.ifc.riodosul.repositoriocientifico.helper;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;

public class Email {
	
	public static void sendEmail(String host, String port,final String username, final String password, 
		String to, String subject, String message) throws AddressException, MessagingException {
	
		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
 
		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
			}
		};
 
		Session session = Session.getInstance(properties, auth);
 
		// creates a new e-mail message
		Message msg = new MimeMessage(session);
 
		msg.setFrom(new InternetAddress(username));
		InternetAddress[] toAddresses = { new InternetAddress(to) };
		msg.setRecipients(Message.RecipientType.TO, toAddresses);
		msg.setSubject(subject);
		msg.setSentDate(new Date());
		msg.setText(message);
//		msg.setContent(message, "UTF-8");
 
		// sends the e-mail
		Transport.send(msg);
	}
}
