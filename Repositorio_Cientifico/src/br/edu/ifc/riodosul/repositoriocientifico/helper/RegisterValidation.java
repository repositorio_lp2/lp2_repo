package br.edu.ifc.riodosul.repositoriocientifico.helper;

import br.edu.ifc.riodosul.repositoriocientifico.cryptography.Md5Hash;
import br.edu.ifc.riodosul.repositoriocientifico.dao.UserDAO;
import br.edu.ifc.riodosul.repositoriocientifico.model.User;

/* This class is used to verify the data passed by the user at the registration page */
public class RegisterValidation extends GenericValidation {
	
	private User user;
	private String confirmPassword;
	private String oldPassword;
	private final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	private final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=*])(?=\\S+$).{8,}$";
	private final String USER_REGEX = "^[a-zA-Z]+([a-zA-Z0-9]*_?)*{6,33}$";
	private final int NAME_MIN_LENGTH = 3;
	private final int NAME_MAX_LENGTH = 45;
	private final int SURNAME_MIN_LENGTH = 3;
	private final int SURNAME_MAX_LENGTH = 45;
	private boolean validReg;
	private boolean registerUpdate;
	private boolean newPassword;
	
	public RegisterValidation () {
		this.setValidReg(true);
		this.setRegisterUpdate(false);
	}

	public boolean validate() {
		validateName();
		validateSurName();
		if (isRegisterUpdate()) {
			validateOldAndnewPasswords();
			if (isOldEmailChanged()) {
				validateEmail();
			}
		}
		else {
			validateUsername();
			validatePassword();
			validateEmail();
		}
		return isValidReg();
	}
	
	private boolean isOldEmailChanged() {
		UserDAO userDAO = new UserDAO(); 
		return !userDAO.findById(user.getId()).getEmail().equals(user.getEmail());
	}

	private void validateOldAndnewPasswords() {
		if (isNullOrEmpty(getOldPassword())) {
			addMessage("A senha atual não pode ficar em branco.");
			setValidReg(false);
		}
		else { 
			UserDAO userDAO = new UserDAO(); 
			String oldPasswordHash = userDAO.findById(user.getId()).getPassword();
			if (!Md5Hash.verifyPasswordMd5(getOldPassword(), oldPasswordHash)) {
				addMessage("A senha passada não coincide com a senha atual.");
				setValidReg(false);
			}
		}
		/* TODO Determine whether the user pressed that button*/
		if (isNewPassword()) {
			validatePassword();
		}
		else {
			user.setPassword(getOldPassword());
		}
	}

	private void validateName() {
		if (isNullOrEmpty(user.getName())) {
			addMessage("Nome - Campo Vazio");
			setValidReg(false);
		}
		else if (!isStrLenEnough(user.getName(),NAME_MIN_LENGTH, NAME_MAX_LENGTH)) {
			addMessage("Nome - inválido; Tamanho Mínimo: " + NAME_MIN_LENGTH + " caracteres, Tamanho Máximo: " + NAME_MAX_LENGTH + " caracteres.");
			setValidReg(false);
		}
	}
	
	private void validateSurName() {
		if (isNullOrEmpty(user.getSurname())) {
			addMessage("Sobrenome - Campo Vazio");
			setValidReg(false);
		}
		else if (!isStrLenEnough(user.getSurname(), SURNAME_MIN_LENGTH, SURNAME_MAX_LENGTH)) {
			addMessage("Sobrenome - inválido; Tamanho Mínimo: " + SURNAME_MIN_LENGTH + " caracteres, Tamanho Máximo: " + SURNAME_MAX_LENGTH + " caracteres.");
			setValidReg(false);
		}
	}
	
	private void validateUsername() {
		if (isNullOrEmpty(user.getLogin())) {
			addMessage("Nome de usuário em branco.");
			setValidReg(false);
		}
		else if (!user.getLogin().matches(USER_REGEX)) {
			addMessage("Nome de usuário deve começar com letra, não ter espaços nem caracteres especiais, e ao menos 6 caracteres");
			setValidReg(false);
		}
		else {
			UserDAO userDAO = new UserDAO(); 

			if (userDAO.findByField("login", user.getLogin()) != null) {
				addMessage("Nome de usuário em uso.");
				setValidReg(false);
			}
		}
	}
	
	private void validatePassword() {
		if (isNullOrEmpty(user.getPassword()) && isNullOrEmpty(confirmPassword)) {
			addMessage("A senha não pode ficar em branco.");
			setValidReg(false);
		}
		else if (!user.getPassword().matches(PASSWORD_REGEX)) {
			addMessage("Senha inválida.\nA senha deve ser composta por 8 characteres e conter ao menos um character especial, uma letra maiúscula, uma letra minúscula e um número.");
			setValidReg(false);
		}
		else if (!confirmPassword.equals(user.getPassword())) {
			addMessage("A senha de confirmação não coincide.");
			setValidReg(false);

		}
	}
		
	private void validateEmail() {
		if (isNullOrEmpty(user.getEmail())) {
				addMessage("E-mail não pode ficar em branco.");
				setValidReg(false);
		}
		else if (!user.getEmail().matches(EMAIL_REGEX)) {
			addMessage("E-mail inválido.");
			setValidReg(false);
		}
		else {
			UserDAO userDAO = new UserDAO(); 
	
			if (userDAO.findByField("email", user.getEmail().replaceAll("\\s","")) != null) {
				addMessage("E-mail já cadastrado.");
				setValidReg(false);
			}
		}
	}
		
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public boolean isValidReg() {
		return validReg;
	}

	public void setValidReg(boolean validReg) {
		this.validReg = validReg;
	}
	public boolean isRegisterUpdate() {
		return registerUpdate;
	}

	public void setRegisterUpdate(boolean registerUpdate) {
		this.registerUpdate = registerUpdate;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public boolean isNewPassword() {
		return newPassword;
	}

	public void setNewPassword(boolean newPassword) {
		this.newPassword = newPassword;
	}
}
