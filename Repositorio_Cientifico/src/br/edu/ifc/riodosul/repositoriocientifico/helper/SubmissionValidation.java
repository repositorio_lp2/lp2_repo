package br.edu.ifc.riodosul.repositoriocientifico.helper;

import org.apache.commons.fileupload.FileItem;
import br.edu.ifc.riodosul.repositoriocientifico.model.Document;

public class SubmissionValidation extends GenericValidation{
	private Document document = new Document();
	
	private boolean validSub;
	private boolean isUpdate = false;
	private boolean haveFileToUpdate = false;
	private final int TITLE_MIN_LENGTH = 3;
	private final int TITLE_MAX_LENGTH = 512;
	private final int DESCRIPTION_MIN_LENGTH = 10;
	private final int DESCRIPTION_MAX_LENGTH = 255;
	private final int ABSTRACT_MIN_LENGTH = 50;
	private final int ABSTRACT_MAX_LENGTH = 4096;
	private String fileType;
	private FileItem item;
	
	
	public SubmissionValidation() {
		this.validSub = true;
	}
	
	public boolean validate() {
		validateTitle();
		validateDescription();
		validateAbstract();
		validateAuthors();
		validateType();
		validateFileItem();
		validateFileType();
		return isValidSub();
	}
	
	private void validateTitle() {
		if (isNullOrEmpty(document.getTitle())) {
			addMessage("Título - Campo Vazio");
			setValidSub(false);
		}
		else if (!isStrLenEnough(document.getTitle(), TITLE_MIN_LENGTH, TITLE_MAX_LENGTH)) {
			addMessage("Titulo - inválido; Tamanho Mínimo: " + TITLE_MIN_LENGTH + " caracteres, Tamanho Máximo: " + TITLE_MAX_LENGTH + " caracteres.");
			setValidSub(false);
		}
	}
	
	private void validateDescription() {
		if (isNullOrEmpty(document.getDescription())) {
			addMessage("Descrição - Campo Vazio");
			setValidSub(false);
		}
		else if (!isStrLenEnough(document.getDescription(), DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH)) {
			addMessage("Descrição - inválida; Tamanho Mínimo: " + DESCRIPTION_MIN_LENGTH + " caracteres, Tamanho Máximo: " + DESCRIPTION_MAX_LENGTH + " caracteres.");
			setValidSub(false);
		}
	}
	
	private void validateAbstract() {
		if (isNullOrEmpty(document.getResume())) {
			addMessage("Resumo - Campo Vazio");
			setValidSub(false);
		}
		else if (!isStrLenEnough(document.getResume(), ABSTRACT_MIN_LENGTH, ABSTRACT_MAX_LENGTH)) {
			addMessage("Resumo - inválido; Tamanho Mínimo: " + ABSTRACT_MIN_LENGTH + " caracteres, Tamanho Máximo: " + ABSTRACT_MAX_LENGTH + " caracteres.");
			setValidSub(false);
		}
	}

	private void validateFileType() {
		if ( !(getItem() == null) && !getItem().getName().isEmpty()) {
			if (!getFileType().equals("application/pdf")) {
				addMessage("Tipo de arquivo inválido. Apenas .pdf é aceito.");
				setValidSub(false);
			}
		}
	}
	
	private void validateFileItem() {
		System.out.println(getItem().getName().isEmpty());
		if (getItem() == null || getItem().getName().isEmpty()) {
			if (!this.isUpdate()) {
				addMessage("O arquivo não foi submetido.");
				setValidSub(false);
			}
		}
	}
	
	private void validateAuthors() {
		if(document.getAuthors().size() == 0) {
			addMessage("É necessário adicionar um Autor.");
			setValidSub(false);
		}
	}
	
	private void validateType() {
		if (document.getType() == null || isNullOrEmpty(document.getType().getDescription())) {
			addMessage("É necessário selecionar uma categoria.");
			setValidSub(false);
		}
	}
		
	public boolean isValidSub() {
		return validSub;
	}

	public void setValidSub(boolean validSub) {
		this.validSub = validSub;
	}
	
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	public FileItem getItem() {
		return item;
	}

	public void setItem(FileItem item) {
		this.item = item;
	}
	public boolean isUpdate() {
		return isUpdate;
	}

	public void setUpdate(boolean isUpdate) {
		this.isUpdate = isUpdate;
	}

	public boolean isHaveFileToUpdate() {
		return haveFileToUpdate;
	}

	public void setHaveFileToUpdate(boolean haveFileToUpdate) {
		this.haveFileToUpdate = haveFileToUpdate;
	}

}
