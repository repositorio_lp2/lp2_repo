package br.edu.ifc.riodosul.repositoriocientifico.helper;

public class SearchValidation extends GenericValidation {
	
	private final int TITLE_MIN_LENGTH = 1;
	private final int TITLE_MAX_LENGTH = 512;
	private final int SEARCH_TEXT_INPUT_MIN_LENGTH = 1;
	private final int SEARCH_TEXT_INPUT_MAX_LENGTH = 1024;
	private final int AUTHOR_NAME_MIN_LENGTH = 3;
	private final int AUTHOR_NAME_MAX_LENGTH = 45;
	private final int KEYWORD_MIN_LENGTH = 3;
	private final int KEYWORD_MAX_LENGTH = 32;
	private final int DISCIPLINE_MIN_LENGTH = 3;
	private final int DISCIPLINE_MAX_LENGTH = 64;
	private final String DATE_REGEX = "[0-9]{1,2}(/|-)[0-9]{1,2}(/|-)[0-9]{4}";
	private boolean validSearchForm;
	private SearchForm searchForm; 
	
	public SearchValidation() {
		this.setValidSearchForm(true);
	}
	
	public boolean validate() {
		
		/*Por enquanto se nenhum checkbox estiver ativado ele apenas checa o texto da pesquisa normal, caso contrario ele ignora*/
		if (!searchForm.hasAnyAdvancedField()) {
			if(!searchForm.hasMainSearch()) {
				addMessage("Todos os campos estão vazios");
				this.setValidSearchForm(false);
			}
			else {
				validateSearchTextInput();
			}	
		}
		else {
			validateSearchTextInput();
			validateTitle();
			validateAuthors();
			validateKeywords();
			validateDisciplines();
			validateStartDate();
			validateFinalDate();
		//	validateBetweenDates();
		}
		return isValidSearchForm();
	}

	// This method validates the main input
	private void validateSearchTextInput() {
		if (searchForm.hasMainSearch()) {
			boolean isValidInput = validateInputNullOrEmptyAndLength (searchForm.getSearchTextInput(), "Texto", SEARCH_TEXT_INPUT_MIN_LENGTH, SEARCH_TEXT_INPUT_MAX_LENGTH);
			this.setValidSearchForm(isValidInput && isValidSearchForm());
		}
	}

	private void validateTitle() {
		boolean isValidInput = true;
		
		if (searchForm.hasTitle()) {
			validateInputNullOrEmptyAndLength (searchForm.getTitle(), "Título", TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);		
			System.out.println(isValidInput);
			this.setValidSearchForm(isValidInput && isValidSearchForm());
		}
	}
	
	private void validateAuthors() {
		if (searchForm.hasAuthors()) {
			boolean isValidInput = true;

			if (searchForm.getAuthors() != null) {
				for (String authorName : searchForm.getAuthors()) {
					isValidInput = isValidInput && validateInputNullOrEmptyAndLength (authorName, "Nome do autor - " + authorName + ":", AUTHOR_NAME_MIN_LENGTH, AUTHOR_NAME_MAX_LENGTH);
				}
			}
			else {
				addMessage("Autores em branco.");
				isValidInput = false;
			}
			this.setValidSearchForm(isValidInput);
		}
	}
	
	
	private void validateKeywords() {
		
		if (searchForm.hasKeywords()) {
			boolean isValidInput = true;
			
			if (searchForm.getKeywords() != null ) {
				for (String keyword : searchForm.getKeywords()) {
					isValidInput = isValidInput && validateInputNullOrEmptyAndLength (keyword, "Palavra Chave - " + keyword + ":", KEYWORD_MIN_LENGTH, KEYWORD_MAX_LENGTH);
				}
			}
			else {
				addMessage("Palavras chave em branco.");
				isValidInput = false;
			}
			this.setValidSearchForm(isValidInput);
		}
	}
	
	private void validateDisciplines() {
		
		
		if (searchForm.hasDisciplines()) {
			boolean isValidInput = true;
			
			if (searchForm.getDisciplines() != null ) {
				for (String discipline : searchForm.getDisciplines()) {
					isValidInput = isValidInput && validateInputNullOrEmptyAndLength (discipline, "Disciplina - " + discipline + ":", DISCIPLINE_MIN_LENGTH, DISCIPLINE_MAX_LENGTH);
				}
			}
			else {
				addMessage("Palavras chave em branco.");
				isValidInput = false;
		
			}
			this.setValidSearchForm(isValidInput);
		}
	}
	
	private void validateStartDate() {
		if (searchForm.hasStartDate()) {
			if (!isNullOrEmpty(searchForm.getStartDate())) {
				if (!searchForm.getStartDate().matches(DATE_REGEX)) {
					addMessage("Data Inicial - Formato Inválido");
					this.setValidSearchForm(false);
				}
			}
			else {
				addMessage("Data Inicial não informada");
				this.setValidSearchForm(false);
			}
		}
	}
	
	private void validateFinalDate() {
		if (searchForm.hasFinalDate()) {
			if (!isNullOrEmpty(searchForm.getFinalDate())) {
				if (!searchForm.getFinalDate().matches(DATE_REGEX)) {
					addMessage("Data Final - Formato Inválido");
					this.setValidSearchForm(false);
				}
			}
			else {
				addMessage("Data Final não informada");
				this.setValidSearchForm(false);
			}
		}
	}

	/* This methods validates whether the start 
	 * date is less than or equal to the end date 
	 * the start and final validates must occur 
	 * before the call to this method */ 
	/*private void validateBetweenDates() {
		if (searchForm.isStartDateCheckBox() && searchForm.isFinalDateCheckBox()) {
			if (isValidSearchForm()) {
				Date startDate = null;
				try {
					startDate = new SimpleDateFormat("yyyy-MM-dd").parse(searchForm.getStartDate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Date finalDate= null;;
				try {
					finalDate = new SimpleDateFormat("yyyy-MM-dd").parse(searchForm.getFinalDate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
				if (Timestamp.valueOf(formatter.format(startDate)).compareTo(Timestamp.valueOf(formatter.format(finalDate))) > 0) {
					addMessage("A data inicial é maior que a data final.");
					System.out.println(Timestamp.valueOf(formatter.format(startDate) + "00:00:00"));
					System.out.println(Timestamp.valueOf(formatter.format(finalDate) + "00:00:00"));
					this.setValidSearchForm(false);
				}
			}
		}
	}*/


	
	
	public boolean isValidSearchForm() {
		return validSearchForm;
	}

	public void setValidSearchForm(boolean validSearchForm) {
		this.validSearchForm = validSearchForm;
	}
	

	public SearchForm getSearchForm() {
		return searchForm;
	}

	public void setSearchForm(SearchForm searchForm) {
		this.searchForm = searchForm;
	}
}
