package br.edu.ifc.riodosul.repositoriocientifico.helper;

import java.util.ArrayList;
import java.util.List;

/* This class is used for generic methods to validate forms. 
 * 
 */

public class GenericValidation {

	private List<String> message;
	public GenericValidation() {
		this.message = new ArrayList<String>();
	}
	
	protected boolean validateInputNullOrEmptyAndLength (String input, String fieldName,int lowerBound, int upperBound) {
		if (isNullOrEmpty(input)) {
			addMessage(fieldName + " - Campo Vazio");
			return false;
		}
		else if (!isStrLenEnough(input, lowerBound, upperBound)) {
			addMessage(fieldName + " - inválido; Tamanho Mínimo: " + lowerBound + " caracteres, Tamanho Máximo: " + upperBound + " caracteres.");
			return false;
		}
		else {
			return true;
		}
	}
	
	protected boolean isNullOrEmpty(String str) {
		return str == null || str.isEmpty();
	}
	
	protected boolean isStrLenEnough(String str,  int lowerBound, int upperBound) {
		return str.length() >= lowerBound && str.length() <= upperBound;
	}
	
	public void addMessage(String message) {
		this.message.add(message);
	}
	
	public List<String> getMessage() {
		return message;
	}
	
	public boolean validateRecaptcha(String recaptcha) {
		boolean resp = false;
		try {
			if(CaptchaValidation.verify(recaptcha)) {
				resp = true;
			} else {
				message.add("Verifique sua identidade no campo 'Não sou robô'");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
}
