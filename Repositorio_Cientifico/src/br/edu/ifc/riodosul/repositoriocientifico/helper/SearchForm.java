package br.edu.ifc.riodosul.repositoriocientifico.helper;

import java.util.List;

public class SearchForm {

	private String searchTextInput;
	private String title;
	private List<String> authors;
	private List<String> keywords;
	private List<String> disciplines;
	private String startDate;
	private String finalDate;

	private boolean hasMainSearch;
	private boolean hasTitle;
	private boolean hasAuthors;
	private boolean hasKeywords;
	private boolean hasDisciplines;
	private boolean hasStartDate;
	private boolean hasFinalDate;

	public SearchForm() {
		this.hasMainSearch = false;
		this.hasTitle = false;
		this.hasAuthors = false;
		this.hasKeywords = false;
		this.hasDisciplines = false;
		this.hasStartDate = false;
		this.hasFinalDate = false;
	}

	public boolean hasAnyAdvancedField() {
		 return this.hasAuthors()
		 || this.hasTitle()
		 || this.hasDisciplines()
		 || this.hasKeywords()
		 || this.hasStartDate()
		 || this.hasFinalDate();
	}

	public String getSearchTextInput() {
		return searchTextInput;
	}

	public void setSearchTextInput(String searchTextInput) {
		this.searchTextInput = searchTextInput;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public List<String> getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(List<String> disciplines) {
		this.disciplines = disciplines;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(String endDate) {
		this.finalDate = endDate;
	}

	public boolean hasMainSearch() {
		return hasMainSearch;
	}

	public void setHasMainSearch(boolean hasMainSearch) {
		this.hasMainSearch = hasMainSearch;
	}

	public boolean hasTitle() {
		return hasTitle;
	}

	public void setHasTitle(boolean hasTitle) {
		this.hasTitle = hasTitle;
	}

	public boolean hasAuthors() {
		return hasAuthors;
	}

	public void setHasAuthors(boolean hasAuthors) {
		this.hasAuthors = hasAuthors;
	}

	public boolean hasKeywords() {
		return hasKeywords;
	}

	public void setHasKeywords(boolean hasKeywords) {
		this.hasKeywords = hasKeywords;
	}

	public boolean hasDisciplines() {
		return hasDisciplines;
	}

	public void setHasDisciplines(boolean hasDisciplines) {
		this.hasDisciplines = hasDisciplines;
	}

	public boolean hasStartDate() {
		return hasStartDate;
	}

	public void setHasStartDate(boolean hasStartDate) {
		this.hasStartDate = hasStartDate;
	}

	public boolean hasFinalDate() {
		return hasFinalDate;
	}

	public void setHasFinalDate(boolean hasFinalDate) {
		this.hasFinalDate = hasFinalDate;
	}

	// public boolean isSearchTextInputOn() {
	// return searchTextInputOn;
	// }
	//
	// public void setSearchTextInputOn(boolean searchTextInputOn) {
	// this.searchTextInputOn = searchTextInputOn;
	// }

}