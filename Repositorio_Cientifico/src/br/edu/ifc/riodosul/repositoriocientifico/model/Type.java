package br.edu.ifc.riodosul.repositoriocientifico.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="type")
public class Type {
	
	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String description;
	
	@OneToMany(cascade = { 
			CascadeType.PERSIST,
			CascadeType.MERGE,
			CascadeType.REMOVE
		},
		mappedBy = "type",
		orphanRemoval = true,
		fetch = FetchType.EAGER
	)
	private Set<Document> documents = new HashSet<Document>();
	
	public void addDocument(Document document) {
        documents.add(document);
        document.setType(this);
    }
 
    public void removeDocument(Document document) {
        documents.remove(document);
        document.setType(null);
    }
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}