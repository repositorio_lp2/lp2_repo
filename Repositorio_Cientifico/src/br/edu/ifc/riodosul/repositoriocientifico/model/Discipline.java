package br.edu.ifc.riodosul.repositoriocientifico.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "discipline")
public class Discipline {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String name;

	@ManyToMany(mappedBy = "disciplines",
			fetch = FetchType.EAGER)
	private Set<User> users = new HashSet<User>();
	
	@ManyToMany(mappedBy = "disciplines",
			fetch = FetchType.EAGER)
	private Set<Document> documents = new HashSet<Document>();

	public Set<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) { 
			return true;
		}
		else if (!(o instanceof Discipline)) {
			return false;
		}
		else {
			return id != null && id.equals(((Discipline) o).id);
		}
	}	
	@Override
	public int hashCode() {
		return 31;
	}
}
