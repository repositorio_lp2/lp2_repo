package br.edu.ifc.riodosul.repositoriocientifico.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String login;
	
	@Column
	private String password;
	
	@Column
	private String name;
	
	@Column
	private String surname;
	
	@Column
	private String email;
	
	@Column
	private boolean active;	
	
	@Column
	private boolean deleted;

	@Column (name = "access_level")
	private int accessLevel;
	
	@Column (name = "creation_date")
	private Timestamp creationDate;
	
	@OneToMany(cascade = { 
			CascadeType.PERSIST,
			CascadeType.MERGE,
			CascadeType.REMOVE
		},
		mappedBy = "user",
		orphanRemoval = true,
		fetch = FetchType.EAGER
	)
	private Set<Role> roles = new HashSet<Role>();

	@ManyToMany(cascade = { 
			CascadeType.PERSIST, 
			CascadeType.MERGE
	},
	fetch = FetchType.EAGER)
	
	@JoinTable(name = "user_has_discipline",
		joinColumns = @JoinColumn(name = "user_id"),
		inverseJoinColumns = @JoinColumn(name = "discipline_id")
	)
	private Set<Discipline> disciplines = new HashSet<Discipline>();
	
	@OneToMany(cascade = { CascadeType.ALL },
		mappedBy = "user",
		orphanRemoval = true,
		fetch = FetchType.EAGER
	)
	private Set<Document> documents = new HashSet<Document>();
	
	public void addDocument(Document document) {
        documents.add(document);
        document.setUser(this);
    }
 
    public void removeDocument(Document document) {
        documents.remove(document);
        document.setUser(null);
    }
	
	public void addDisciplines(Discipline userDiscipline) {
		disciplines.add(userDiscipline);
		userDiscipline.getUsers().add(this);
	}
	 
	public void removeDisciplines(Discipline userDiscipline) {
		disciplines.remove(userDiscipline);
		userDiscipline.getUsers().remove(this);
	}
	
	public Set<Discipline> getDisciplines() {
		return disciplines;
	}
		
	public Set<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

	public void setDisciplines(Set<Discipline> disciplines) {
		this.disciplines = disciplines;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public int getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(int accessLevel) {
		this.accessLevel = accessLevel;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { 
			return true;
		}
		else if (!(o instanceof User)) {
			return false;
		}
		else {
			return id != null && id.equals(((User) o).id);
		}
	}	
	
	@Override
	public int hashCode() {
		return 31;
	}
}
