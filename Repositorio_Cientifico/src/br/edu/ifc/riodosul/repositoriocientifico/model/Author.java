package br.edu.ifc.riodosul.repositoriocientifico.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "author")
public class Author {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private String surname;
	
	@Column
	private String email;
	
	@ManyToMany(mappedBy = "authors",
			fetch = FetchType.EAGER)
	private Set<Document> documents = new HashSet<Document>();

	public Set<Document> getDocuments() {
		return documents;
	}
	
	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) { 
			return true;
		}
		else if (!(o instanceof Class)) {
			return false;
		}
		else {
			return id != null && id.equals(((Author) o).id);
		}
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
}
