package br.edu.ifc.riodosul.repositoriocientifico.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "document")
public class Document {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String path;
	
	@Column
	private String title;
	
	@Column
	private String description;
	
	@Column (name = "date_post")
	private Timestamp datePost;
	
	@Column (name = "date_last_change")
	private Timestamp dateLastChange;
	
	@Column (name = "abstract")
	private String resume;
	
	@ManyToMany(cascade = { 
			CascadeType.PERSIST, 
			CascadeType.MERGE
	},
	fetch = FetchType.EAGER)
	@JoinTable(name = "document_has_author",
		joinColumns = @JoinColumn(name = "document_id"),
		inverseJoinColumns = @JoinColumn(name = "author_id")
	)
	private Set<Author> authors = new HashSet<Author>();
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id")
    private Type type;
	
	@OneToMany(cascade = { 
			CascadeType.PERSIST,
			CascadeType.MERGE,
			CascadeType.REMOVE
		},
		mappedBy = "document",
		orphanRemoval = true,
		fetch = FetchType.EAGER
	)
	private Set<Keyword> keywords = new HashSet<Keyword>();
	
	@ManyToMany(cascade = { 
			CascadeType.PERSIST, 
			CascadeType.MERGE
	},
	fetch = FetchType.EAGER)
	@JoinTable(name = "document_has_discipline",
			joinColumns = @JoinColumn(name = "document_id"),
			inverseJoinColumns = @JoinColumn(name = "discipline_id")
		)
	private Set<Discipline> disciplines = new HashSet<Discipline>();
	
	public void addDiscipline(Discipline discipline) {
		disciplines.add(discipline);
		discipline.getDocuments().add(this);
    }
 
    public void removeDiscipline(Discipline discipline) {
    	disciplines.remove(discipline);
    	discipline.getDocuments().remove(this);
    }
    
    public Set<Discipline> getDisciplines() {
		return disciplines;
	}

	public void setDisciplines(Set<Discipline> disciplines) {
		this.disciplines = disciplines;
	}
	
	public void addKeyword(Keyword keyword) {
		keywords.add(keyword);
		keyword.setDocument(this);
    }
 
    public void removeKeyword(Keyword keyword) {
    	keywords.remove(keyword);
    	keyword.setDocument(null);
    }
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(Set<Keyword> keywords) {
		this.keywords = keywords;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public void addAuthor(Author author) {
		authors.add(author);
		author.getDocuments().add(this);
	}
	 
	public void removeAuthor(Author author) {
		authors.remove(author);
		author.getDocuments().remove(this);
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Timestamp getDatePost() {
		return datePost;
	}
	
	public void setDatePost(Timestamp datePost) {
		this.datePost = datePost;
	}
	
	public Timestamp getDateLastChange() {
		return dateLastChange;
	}
	
	public void setDateLastChange(Timestamp dateLastChange) {
		this.dateLastChange = dateLastChange;
	}
	
	public String getResume() {
		return resume;
	}
	
	public void setResume(String resume) {
		this.resume = resume;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) { 
			return true;
		}
		else if (!(o instanceof User)) {
			return false;
		}
		else {
			return id != null && id.equals(((Document) o).id);
		}
	}	
	
	@Override
	public int hashCode() {
		return 31;
	}
}
