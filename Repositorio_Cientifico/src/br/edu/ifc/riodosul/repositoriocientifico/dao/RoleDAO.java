package br.edu.ifc.riodosul.repositoriocientifico.dao;

import br.edu.ifc.riodosul.repositoriocientifico.model.Role;

public class RoleDAO extends GenericDAO<Role>{

	public RoleDAO() {
		super(Role.class);
	}
}
