package br.edu.ifc.riodosul.repositoriocientifico.dao;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import br.edu.ifc.riodosul.repositoriocientifico.helper.SearchForm;
import br.edu.ifc.riodosul.repositoriocientifico.model.Document;

public class DocumentDAO extends GenericDAO<Document> {
	private EntityManagerFactory emf;
	private EntityManager em;

	
	public DocumentDAO() {
		super(Document.class);
		emf = Persistence.createEntityManagerFactory("AcademicRepository");
	}

    @SuppressWarnings("unchecked")
	public List<Document> advancedSearch(SearchForm searchForm) {
        em = emf.createEntityManager();
        try {
            StringBuilder builder = new StringBuilder();
            builder.append("SELECT doc FROM ");
            builder.append("Document");
            builder.append(" doc");
            
            if (searchForm.hasAuthors()) {
            	List<String> authors = searchForm.getAuthors();
            	Iterator<String> iter = authors.iterator();

            	builder.append(" INNER JOIN doc.authors a with a.name = ");            	
            	while (iter.hasNext()) {
            		String a = iter.next();
            		builder.append("'" + a + "'");
            		if (iter.hasNext()) {
            			builder.append(" OR a.name = ");
            		}
            	}
            }
            
            if (searchForm.hasKeywords()) {
            	List<String> keywords = searchForm.getKeywords();
            	Iterator<String> iter = keywords.iterator();

            	builder.append(" INNER JOIN doc.keywords k with k.word = ");
            	while (iter.hasNext()) {
            		String k = iter.next();
            		builder.append("'" + k + "'");
            		if (iter.hasNext()) {
            			builder.append(" OR k.word =  ");
            		}
            	}
            }
            
            if (searchForm.hasDisciplines()) {
            	List<String> disciplines = searchForm.getDisciplines();
            	Iterator<String> iter = disciplines.iterator();

            	builder.append(" INNER JOIN doc.disciplines d with d.name = ");
            	while (iter.hasNext()) {
            		String d = iter.next();
            		builder.append("'" + d + "'");
            		if (iter.hasNext()) {
            			builder.append(" OR d.name =  ");
            		}
            	}
            }
            
            if (searchForm.hasStartDate()) {
            	builder.append(" WHERE doc.datePost >= CONCAT(date_format(str_to_date('" + searchForm.getStartDate() + "', '%d/%m/%Y')" +",'%Y/%m/%d'), ' 00:00:00')");
            	if (searchForm.hasFinalDate()) {
            		builder.append(" AND doc.datePost <= CONCAT(date_format(str_to_date('" + searchForm.getFinalDate() + "', '%d/%m/%Y')" +",'%Y/%m/%d'), ' 23:59:59')");
            	}
            }
            else if (searchForm.hasFinalDate()) {
            	builder.append(" WHERE doc.datePost <= CONCAT(date_format(str_to_date('" + searchForm.getFinalDate() + "', '%d/%m/%Y')" +",'%Y/%m/%d'), ' 23:59:59')");
            }
            if (searchForm.hasTitle()) {
	            if (searchForm.hasStartDate() || searchForm.hasFinalDate()) {
	            	builder.append(" AND doc.title like '%" + searchForm.getTitle() + "%'");
	            }
	            else {
	            	builder.append(" WHERE doc.title like '%" + searchForm.getTitle() + "%'");
	            }
	            if (searchForm.hasMainSearch()) {
	            	builder.append(" OR doc.title like '%" + searchForm.getSearchTextInput() + "%'");
	            }
            }
            else if (searchForm.hasMainSearch()) {
            	if (searchForm.hasStartDate() || searchForm.hasFinalDate()) {
            		builder.append(" AND doc.title like '%" + searchForm.getSearchTextInput() + "%'");
	            }
            	else {
            		builder.append(" WHERE doc.title like '%" + searchForm.getSearchTextInput() + "%'");
	            }
            }
            System.out.println(builder.toString());
            return em.createQuery(builder.toString()).getResultList();
        } catch (Exception e) {
            System.err.println(e);
            return null;
        } finally {
            em.close();
        }
    }
    
    /**
     * This method is poorly executed, and may be corrected in the future
     * @param document
     * @return boolean = true if everything went ok, false otherwise
     */
    public boolean delete(Document document) {
        em = emf.createEntityManager();
        try {
        	em.getTransaction().begin();
            StringBuilder builder = new StringBuilder();
            builder.append("DELETE FROM Document");
            builder.append(" WHERE id = " + document.getId());

            em.createQuery(builder.toString()).executeUpdate();
            em.flush();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.err.println(e);
            return false;
        } finally {
            em.close();
        }
    }
    
	public Timestamp findByLastDateAndUserId(Long userId) {
        EntityManager em = emf.createEntityManager();
        try {
            StringBuilder builder = new StringBuilder();
            builder.append("SELECT MAX(doc.datePost) FROM ");
            builder.append(" Document doc");
            builder.append(" WHERE doc.user = " + userId); 
            System.out.println(builder);
            return (Timestamp) em.createQuery(builder.toString()).getSingleResult();
        } catch (Exception e) {
        	System.err.println(e);
            return null;
        } finally {
            em.close();
        }
	}
}
