package br.edu.ifc.riodosul.repositoriocientifico.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.ifc.riodosul.repositoriocientifico.model.Type;

public class TypeDAO extends GenericDAO<Type> {
	private EntityManagerFactory emf;
	private EntityManager em;
	
	public TypeDAO(){
		super(Type.class);
		emf = Persistence.createEntityManagerFactory("AcademicRepository");
	}
	
	public boolean delete(Type type) {
        em = emf.createEntityManager();
        try {
        	em.getTransaction().begin();
            StringBuilder builder = new StringBuilder();
            builder.append("DELETE FROM Type");
            builder.append(" WHERE id = " + type.getId());

            em.createQuery(builder.toString()).executeUpdate();
            em.flush();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.err.println(e);
            return false;
        } finally {
            em.close();
        }
    }
}
