package br.edu.ifc.riodosul.repositoriocientifico.dao;

import br.edu.ifc.riodosul.repositoriocientifico.model.Discipline;

public class DisciplineDAO extends GenericDAO<Discipline> {
	
	public DisciplineDAO() {
		super(Discipline.class);
	}
}
