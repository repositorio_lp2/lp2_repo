package br.edu.ifc.riodosul.repositoriocientifico.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.ifc.riodosul.repositoriocientifico.model.User;

public class UserDAO extends GenericDAO<User> {
	private EntityManagerFactory emf;
	private EntityManager em;
	
	public UserDAO() {
		super(User.class);
		emf = Persistence.createEntityManagerFactory("AcademicRepository");
	}
	
	public boolean delete(User user) {
        em = emf.createEntityManager();
        try {
        	em.getTransaction().begin();
            StringBuilder builder = new StringBuilder();
            builder.append("DELETE FROM User");
            builder.append(" WHERE id = " + user.getId());

            em.createQuery(builder.toString()).executeUpdate();
            em.flush();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.err.println(e);
            return false;
        } finally {
            em.close();
        }
    }
}
