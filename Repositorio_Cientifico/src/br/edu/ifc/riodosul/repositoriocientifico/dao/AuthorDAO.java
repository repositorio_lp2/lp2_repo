package br.edu.ifc.riodosul.repositoriocientifico.dao;

import br.edu.ifc.riodosul.repositoriocientifico.model.Author;

public class AuthorDAO extends GenericDAO<Author> {
	
	public AuthorDAO () {
		super(Author.class);
	}

}
