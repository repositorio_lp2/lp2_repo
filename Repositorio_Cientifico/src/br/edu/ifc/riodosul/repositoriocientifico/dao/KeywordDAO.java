package br.edu.ifc.riodosul.repositoriocientifico.dao;

import br.edu.ifc.riodosul.repositoriocientifico.model.Keyword;

public class KeywordDAO extends GenericDAO<Keyword> {

	public KeywordDAO() {
		super(Keyword.class);
	}
}
