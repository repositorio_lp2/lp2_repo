package br.edu.ifc.riodosul.repositoriocientifico.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.edu.ifc.riodosul.repositoriocientifico.dao.DocumentDAO;
import br.edu.ifc.riodosul.repositoriocientifico.helper.SearchForm;
import br.edu.ifc.riodosul.repositoriocientifico.helper.SearchValidation;
import br.edu.ifc.riodosul.repositoriocientifico.model.Document;

/**
 * Servlet implementation class SearchDocumentServlet
 */
@WebServlet("/SearchDocumentServlet")
public class SearchDocumentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchDocumentServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String to = "pages/search-result.jsp";
		
		if ((request.getParameter("display-document") != null) && (!request.getParameter("display-document").isEmpty())) {
            String id = request.getParameter("doc-id");
            System.out.println(id);
            if (id != null && !id.isEmpty()) {
            	DocumentDAO documentDAO = new DocumentDAO();
            	Document document = documentDAO.findById(Long.parseLong(id));
            	request.setAttribute("document", document);
            	to = "pages/display-document.jsp";
            }
            else {
            	to = "pages/search-result.jsp";
            }
	    }
		request.setCharacterEncoding("UTF-8");
		if (request.getParameter("submit") != null) {	
			SearchForm searchForm = new SearchForm();
			SearchValidation searchValidation = new SearchValidation();
			
			fillSearchFormObject(request, response, searchForm);
			searchValidation.setSearchForm(searchForm);
			searchValidation.validate();
			if (searchValidation.isValidSearchForm()) {
				/* Aqui deve ser chamado um método da classe query para lidar com a busca */
				DocumentDAO documentDAO = new DocumentDAO();
				List<Document> documents;
				
				if (!searchForm.hasAnyAdvancedField()) {
					documents = documentDAO.search("title", searchForm.getSearchTextInput());
				}
				else {
					documents = documentDAO.advancedSearch(searchForm);
				}
				request.setAttribute("documents", documents);
				to = "pages/search-result.jsp";
				/*RequestDispatcher dispatcher = request
			            .getRequestDispatcher(to);
			    dispatcher.forward(request, response);*/
			}
			else {
				to = "pages/main-page.jsp";
				request.setAttribute("failureMessages", searchValidation.getMessage());
				System.out.println(searchValidation.getMessage());
				/* This peace of code bellow handles the link whose brings the user to the display-document page */
				
				
			/*	RequestDispatcher dispatcher = request
			            .getRequestDispatcher(to);
			    dispatcher.forward(request, response);*/ 
			}
		}
		else {
			//response.sendRedirect(request.getContextPath() + "/" + to);
		}
		RequestDispatcher dispatcher = request
	            .getRequestDispatcher(to);
	    dispatcher.forward(request, response);
	}
	protected void fillSearchFormObject(HttpServletRequest request, HttpServletResponse response, SearchForm searchForm) throws ServletException, IOException {	
		/* This method is used to fill the search form object with the request data */
		
		if (request.getParameter("search-text-input") != null && !request.getParameter("search-text-input").isEmpty()) {
			searchForm.setHasMainSearch(true);
			searchForm.setSearchTextInput(request.getParameter("search-text-input"));
		}
		
		if (request.getParameter("title") != null && !request.getParameter("title").isEmpty()) {
			searchForm.setHasTitle(true);
			searchForm.setTitle(request.getParameter("title"));
		}
		
		/* get inputs from forms, split them by comma and set to the Search form object helper */
		if (request.getParameter("author") != null && !request.getParameter("author").isEmpty()) {
			searchForm.setHasAuthors(true);
			searchForm.setAuthors(Arrays.asList(request.getParameter("author").trim().split(",")));
		}
		
		if (request.getParameter("keyword") != null && !request.getParameter("keyword").isEmpty()) {
			searchForm.setHasKeywords(true);
			searchForm.setKeywords(Arrays.asList(request.getParameter("keyword").trim().split(",")));
		}

		if (request.getParameter("discipline") != null && !request.getParameter("discipline").isEmpty()) {
			searchForm.setHasDisciplines(true);
			searchForm.setDisciplines(Arrays.asList(request.getParameter("discipline").trim().split(",")));
		}
		
		if (request.getParameter("start-date") != null && !request.getParameter("start-date").isEmpty()) {
			searchForm.setHasStartDate(true);
			searchForm.setStartDate(request.getParameter("start-date"));
		}
		
		if (request.getParameter("final-date") != null && !request.getParameter("final-date").isEmpty()) {
			searchForm.setHasFinalDate(true);
			searchForm.setFinalDate(request.getParameter("final-date"));
		}
		
	}
}
