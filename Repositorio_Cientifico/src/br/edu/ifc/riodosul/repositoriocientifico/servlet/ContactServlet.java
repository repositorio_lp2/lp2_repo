package br.edu.ifc.riodosul.repositoriocientifico.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifc.riodosul.repositoriocientifico.dao.UserDAO;
import br.edu.ifc.riodosul.repositoriocientifico.helper.ContactForm;
import br.edu.ifc.riodosul.repositoriocientifico.helper.ContactValidation;
import br.edu.ifc.riodosul.repositoriocientifico.helper.Email;
import br.edu.ifc.riodosul.repositoriocientifico.model.Role;
import br.edu.ifc.riodosul.repositoriocientifico.model.User;

/**
 * Servlet implementation class ContactServlet
 */
@WebServlet("/ContactServlet")
public class ContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String host;
	private String port;
	private String user;
	private String pass;
	 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		final String to = "pages/contact.jsp";
		
		if (request.getParameter("submit") != null) {
			ContactValidation contactValidation = new ContactValidation();
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String subject = request.getParameter("subject");
			String messageContent = request.getParameter("message");
			ContactForm contact = new ContactForm();
			
			contact.setName(name);
			contact.setEmail(email);
			contact.setSubject(subject);
			contact.setMessageContent(messageContent);
			contactValidation.setContact(contact);
			contactValidation.validate();
			
			if (contactValidation.isValidContactData()) {	
				String content = "Nome: " + name + "\n" + "E-mail: " + email + "\n\n" + "Mensagem:\n\n" + messageContent;			
				final String recipient = "eds.riodosul@ifc.edu.br";
				UserDAO userDAO = new UserDAO();
				
				// Envia para um e-mail Fixo. Em seguida para os admins
				sendTo(recipient, content, subject, request);
				for (User user : userDAO.findAll()) {
					for (Role role : user.getRoles()) {
						if (role.getDescription().equalsIgnoreCase("admin")) {
							sendTo(user.getEmail(), content, subject, request);
						}
					}
				}
			}
			else {
				request.setAttribute("contact", contact);
				request.setAttribute("failureMessages", contactValidation.getMessage());
			}
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(to);
		dispatcher.forward(request, response);
    }	
	
	private void sendTo(String recipient, String content, String subject, HttpServletRequest request) {
		String message = "";
		
		try {
			Email.sendEmail(host, port, user, pass, recipient, "Repositório Científico: " + subject, content);
            message = "Mensagem enviada com sucesso.";
		} catch (Exception ex) {
			ex.printStackTrace();
			message = "Mensagem não enviada. Erro: " + ex.getMessage();
		} finally {
			request.setAttribute("successMessage", message);
        }
	}
	
	public void init() {
		// reads SMTP server setting from web.xml file
		ServletContext context = getServletContext();
		host = context.getInitParameter("host");
		port = context.getInitParameter("port");
		user = context.getInitParameter("user");
		pass = context.getInitParameter("pass");
	}
}
