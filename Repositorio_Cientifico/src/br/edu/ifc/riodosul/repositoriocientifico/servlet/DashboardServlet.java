package br.edu.ifc.riodosul.repositoriocientifico.servlet;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifc.riodosul.repositoriocientifico.dao.DocumentDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.RoleDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.TypeDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.UserDAO;
import br.edu.ifc.riodosul.repositoriocientifico.model.Document;
import br.edu.ifc.riodosul.repositoriocientifico.model.Role;
import br.edu.ifc.riodosul.repositoriocientifico.model.Type;
import br.edu.ifc.riodosul.repositoriocientifico.model.User;

/**
 * Servlet implementation class DashboardServlet
 */
@WebServlet("/DashboardServlet")
@ServletSecurity(
        value = @HttpConstraint(
                rolesAllowed = {
                        "admin",
                        "instructor"
                }),
        httpMethodConstraints = {
                @HttpMethodConstraint(value = "GET", rolesAllowed = {
                		"admin",
                        "instructor"
                }),
                @HttpMethodConstraint(value = "POST", rolesAllowed = {
                		"admin",
                        "instructor"
                })
        })
public class DashboardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashboardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String to = "pages/auth/user-dashboard.jsp";
		
		if (request.getParameter("submission") != null) {
			TypeDAO tDao = new TypeDAO();
			request.setAttribute("types", (List<Type>) tDao.findAll());
			to = "pages/auth/submission.jsp";
		}
		else if (request.getParameter("submission-management") != null){
			String login = request.getRemoteUser();
			DocumentDAO documentDAO = new DocumentDAO();
			List<Document> documents = documentDAO.search("user.login", login);
			
			request.setAttribute("documents", documents);
			to = "pages/auth/submission-management.jsp";
			
		}
		else if ((request.getParameter("display-document") != null) && (!request.getParameter("display-document").isEmpty())) {
			to = "SearchDocumentServlet";    
		}
		else if ((request.getParameter("delete") != null) && (!request.getParameter("delete").isEmpty())) {
			 String documentId = request.getParameter("doc-id");
	            
	            if ((documentId != null) && (!documentId.isEmpty())) {	
	            	DocumentDAO documentDAO = new DocumentDAO();
	            	Document document = documentDAO.findById(Long.parseLong(documentId));
	            	
	            	if (!documentDAO.delete(document)) {
	            		request.setAttribute("failureMessage", "O arquivo não pode ser deletado");
	            	} else {
	            		final String textPath = "/home/felipe/git/lp2_repo/Repositorio_Cientifico/WebContent/" + document.getPath() ;
	            		Path path = Paths.get(textPath);
	            	 	try {
	            		    Files.delete(path);
	            		} catch (NoSuchFileException x) {
	            		    System.err.format("%s: no such" + " file or directory%n", path);
	            		} catch (DirectoryNotEmptyException x) {
	            		    System.err.format("%s not empty%n", path);
	            		} catch (IOException x) {
	            		    // File permission problems are caught here.
	            		    System.err.println(x);
	            		}
	            	}
	            		
	            	request.setAttribute("document", document);
	            	to = "pages/display-document.jsp";
	            }
	            else {
	            	to = "pages/search-result.jsp";
	            }
		    }
			else if ((request.getParameter("edit") != null) && (!request.getParameter("edit").isEmpty())) {
				String id = request.getParameter("doc-id");
	            System.out.println(id);
	            if (id != null && !id.isEmpty()) {
	            	DocumentDAO documentDAO = new DocumentDAO();
	            	TypeDAO tDao = new TypeDAO();
	            	
	            	Document document = documentDAO.findById(Long.parseLong(id));
	            	request.setAttribute("document", document);
	            	request.setAttribute("types", (List<Type>) tDao.findAll());
	            	to = "pages/auth/submission.jsp";
	            }
			}
			else if ((request.getParameter("update-register") != null) && (!request.getParameter("update-register").isEmpty())) {
				UserDAO userDAO = new UserDAO();
				String userId = request.getRemoteUser();
				if (userId != null && !userId.isEmpty()) {
					User user = userDAO.findByField("login", userId);
					if (user != null) {
						request.setAttribute("user", user);
						to = "pages/auth/update-register.jsp";
					}
				}
				
			}
			else if ((request.getParameter("submit-update-register") != null) && (!request.getParameter("submit-update-register").isEmpty())) {
				to = "/RegisterUpdateServlet";
			}
			else if (request.getParameter("teaching-management") != null) {
					UserDAO userDAO = new UserDAO();
					List<User> users = userDAO.search("deleted", "0");
					request.setAttribute("users", users);
					to = "pages/auth/admin/admin-manage-teacher.jsp";
			}
			else if (request.getParameter("disable-user") != null && !request.getParameter("disable-user").isEmpty() && request.isUserInRole("admin")) {
				UserDAO userDAO = new UserDAO();
				if (request.getParameter("user-id") != null && !request.getParameter("user-id").isEmpty()) {
					String user_id = request.getParameter("user-id");
					User user = userDAO.findById(Long.parseLong(user_id));
					if (user != null) {
						user.setActive(false);
						userDAO.update(user);
						RoleDAO roleDAO = new RoleDAO();
						for (Role role : user.getRoles()) {
							role.setDescription("disabled_user");
							roleDAO.update(role);
						}
					}
				}
				List<User> users = userDAO.search("deleted", "0");
				request.setAttribute("users", users);
				to = "pages/auth/admin/admin-manage-teacher.jsp";
			}
			else if (request.getParameter("enable-user") != null && !request.getParameter("enable-user").isEmpty() && request.isUserInRole("admin")) {
				UserDAO userDAO = new UserDAO();
				if (request.getParameter("user-id") != null && !request.getParameter("user-id").isEmpty()) {
					String user_id = request.getParameter("user-id");
					User user = userDAO.findById(Long.parseLong(user_id));
					if (user != null) {
						user.setActive(true);
						userDAO.update(user);
						RoleDAO roleDAO = new RoleDAO();
						for (Role role : user.getRoles()) {
							role.setDescription("instructor");
							roleDAO.update(role);
						}
					}
				}
				List<User> users = userDAO.search("deleted", "0");
				request.setAttribute("users", users);
				to = "pages/auth/admin/admin-manage-teacher.jsp";
			}
			else if (request.getParameter("delete-user") != null && !request.getParameter("delete-user").isEmpty() && request.isUserInRole("admin")) {
				UserDAO userDAO = new UserDAO();
				if (request.getParameter("user-id") != null && !request.getParameter("user-id").isEmpty()) {
					String user_id = request.getParameter("user-id");
					User user = userDAO.findById(Long.parseLong(user_id));
					if (user != null) {
						RoleDAO roleDAO = new RoleDAO();
						for (Role role : user.getRoles()) {
							role.setDescription("deleted");
							roleDAO.update(role);
						}
						user.setActive(false);
						user.setDeleted(true);
						userDAO.update(user);
					}
				}
				List<User> users = userDAO.search("deleted", "0");
				request.setAttribute("users", users);
				to = "pages/auth/admin/admin-manage-teacher.jsp";
			}
			else if (request.getParameter("category-management") != null && !request.getParameter("category-management").isEmpty() && request.isUserInRole("admin")) {
				TypeDAO typeDAO = new TypeDAO();
				List<Type> types = typeDAO.findAll();
				request.setAttribute("types", types);
				to = "pages/auth/admin/category-management.jsp";
			}
			else if (request.getParameter("edit-user-submission") != null && !request.getParameter("edit-user-submission").isEmpty() && request.isUserInRole("admin")) {
				if (request.getParameter("user-id") != null && !request.getParameter("user-id").isEmpty()) {
					String userId = request.getParameter("user-id");
					UserDAO userDAO = new UserDAO();
					List<Document> documents = new ArrayList<Document>();
					documents.addAll(userDAO.findById(Long.parseLong(userId)).getDocuments());
					System.out.println("Entrou");
					request.setAttribute("documents", documents);
					to = "pages/auth/submission-management.jsp";	
				}
			}
		//to = "SearchDocumentServlet";    
		
		RequestDispatcher dispatcher = request
	            .getRequestDispatcher(to);
	    dispatcher.forward(request, response);
	}

}
