package br.edu.ifc.riodosul.repositoriocientifico.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

import br.edu.ifc.riodosul.repositoriocientifico.dao.AuthorDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.DocumentDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.TypeDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.UserDAO;
import br.edu.ifc.riodosul.repositoriocientifico.helper.Email;
import br.edu.ifc.riodosul.repositoriocientifico.helper.SubmissionValidation;
import br.edu.ifc.riodosul.repositoriocientifico.model.Author;
import br.edu.ifc.riodosul.repositoriocientifico.model.Discipline;
import br.edu.ifc.riodosul.repositoriocientifico.model.Keyword;
import br.edu.ifc.riodosul.repositoriocientifico.model.Role;
import br.edu.ifc.riodosul.repositoriocientifico.model.Type;
import br.edu.ifc.riodosul.repositoriocientifico.model.User;
import br.edu.ifc.riodosul.repositoriocientifico.model.Document;

/**
 * Servlet implementation class SubmissionServlet
 */
@WebServlet("/SubmissionServlet")
@ServletSecurity(
        value = @HttpConstraint(
                rolesAllowed = {
                        "admin",
                        "instructor"
                }),
        httpMethodConstraints = {
                @HttpMethodConstraint(value = "GET", rolesAllowed = {
                		"admin",
                        "instructor"
                }),
                @HttpMethodConstraint(value = "POST", rolesAllowed = {
                		"admin",
                        "instructor"
                })
        })
public class SubmissionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String UPLOAD_DIRECTORY_VIEW = "uploads";
//	private static final String UPLOAD_DIRECTORY_SAVE = "/home/felipe/git/lp2_repo/Repositorio_Cientifico/WebContent/uploads";
//	private static final String UPLOAD_DIRECTORY_SAVE = "/home/sye/MEGA/college/IFC - Ciência da Computação/Estágio/Projetos/Prof Daniel/lp2_repo/Repositorio_Cientifico/WebContent/uploads/";
	private static final String UPLOAD_DIRECTORY_SAVE = "/home/estagiario/git/lp2_repo/Repositorio_Cientifico/WebContent/uploads/";
	private SubmissionValidation submissionValidation;
	private String recaptchaResult;
	private List<String> authorName = new ArrayList<>();
	private List<String> authorSurname = new ArrayList<>();
	private List<String> authorEmail = new ArrayList<>();
	Long docId;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubmissionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
//		if(request.getRemoteUser() != null && !request.getRemoteUser().isEmpty()) {
//			//request.getRequestDispatcher("/pages/main.jsp").forward(request, response);
//			System.out.println("auth");
//			System.out.println(request.getRemoteUser());
//		} else {
//			//request.getRequestDispatcher("pages/display-document.jsp").forward(request, response);
//			System.out.println("Not auth");
//		}
		
		this.submissionValidation = new SubmissionValidation();
		String to = "pages/auth/submission.jsp";
		TypeDAO tDao = new TypeDAO();
		request.setAttribute("types", (List<Type>) tDao.findAll());
		
		if(ServletFileUpload.isMultipartContent(request)) {
			this.authorEmail = new ArrayList<>();
			this.authorName = new ArrayList<>();
			this.authorSurname = new ArrayList<>();
			setDocumentPropertiesFromRequest(request);
			boolean isUploaded = uploadDocument(request);
			
			if (isUploaded) {
				if (this.submissionValidation.isUpdate()) {
					request.setAttribute("submissionSuccess", "Documento atualizado com sucesso");
					sendEmailToAdmins(submissionValidation.getDocument(), false);
				}
				else {
					request.setAttribute("submissionSuccess", "Submissão realizada com sucesso.");
					sendEmailToAdmins(submissionValidation.getDocument(), true);
				}
			} 
			else {
				request.setAttribute("document", this.submissionValidation.getDocument());
			}
        } 
		else {
			if(request.getParameter("submit") != null) {
				submissionValidation.addMessage("Desculpe esse Servlet lida apenas com requisições de upload de arquivo.");
			}
        }
		
		request.setAttribute("failureMessages", submissionValidation.getMessage());
        request.getRequestDispatcher(to).forward(request, response);
    }
	
	
	private boolean deleteFile(String docPath) {
//		final String textPath = "/home/felipe/git/lp2_repo/Repositorio_Cientifico/WebContent/" + docPath ;
		final String textPath = "/home/sye/MEGA/college/IFC - Ciência da Computação/Estágio/Projetos/Prof Daniel/lp2_repo/Repositorio_Cientifico/WebContent/"  + docPath ;
		Path path = Paths.get(textPath);
	 	try {
		    Files.delete(path);
		} catch (NoSuchFileException x) {
		    System.err.format("%s: no such" + " file or directory%n", path);
		    return false;
		} catch (DirectoryNotEmptyException x) {
		    System.err.format("%s not empty%n", path);
		    return false;
		} catch (IOException x) {
		    // File permission problems are caught here.
		    System.err.println(x);
		    return false;
		}
	 	return true;
	}
	
	private boolean uploadUpdateDocument(HttpServletRequest request) {
		boolean submissionSuccess = false;
		
		UserDAO userDAO = new UserDAO();
		DocumentDAO documentDAO = new DocumentDAO();
    	FileItem toSave = submissionValidation.getItem();
    	
    	submissionValidation.getDocument().setDatePost(documentDAO.findById(this.docId).getDatePost());
    	submissionValidation.getDocument().setDateLastChange(new Timestamp(System.currentTimeMillis()));
    	submissionValidation.getDocument().setUser(userDAO.findByField("login", request.getRemoteUser()));
    	
    	
    	if (toSave.getName() != null && !toSave.getName().isEmpty()) {
    		System.out.println("Primeiro if");
    		
    		boolean fileDeleted = deleteFile(documentDAO.findById(this.docId).getPath());
    		if (!fileDeleted) {
    			submissionValidation.addMessage("O antigo arquivo não pode ser substituído. Por favo contate o administrador.");
    			return false;
    		}
    		
    		submissionValidation.getDocument().setPath(UPLOAD_DIRECTORY_VIEW + File.separator + toSave.getName());
	    	//Insert document into database
	    	try {
				toSave.write(
						new File(UPLOAD_DIRECTORY_SAVE + File.separator + new File(toSave.getName()).getName()));
				AuthorDAO authorDAO = new AuthorDAO();
	        	for (Author a: submissionValidation.getDocument().getAuthors()) {
	        		authorDAO.update(a);
	        	}
				documentDAO.update(submissionValidation.getDocument());
				submissionSuccess = true;
				
				
			} catch (Exception e) {
				submissionValidation.addMessage("Não foi possível gravar o arquivo. Tente novamente " + e.getMessage());
			}
    	}
    	else {
    		submissionValidation.getDocument().setPath(documentDAO.findById(this.docId).getPath());
    		System.out.println("Segundo else");
    		AuthorDAO authorDAO = new AuthorDAO();
        	for (Author a: submissionValidation.getDocument().getAuthors()) {
        		authorDAO.update(a);
        	}
			documentDAO.update(submissionValidation.getDocument());
			submissionSuccess = true;
    	}
    	return submissionSuccess;
	}
	
	private boolean uploadDocument(HttpServletRequest request){
		boolean submissionSuccess = false;
		
		if(this.submissionValidation.getDocument() == null || !this.submissionValidation.validateRecaptcha(this.recaptchaResult)) {
			return submissionSuccess;
			
		}
		
		if(this.submissionValidation.getDocument() != null) {
			this.submissionValidation.validate();
			if (submissionValidation.isValidSub()) {
				
				if (!submissionValidation.isUpdate()) {
				
		        	UserDAO userDAO = new UserDAO();
		        	
		        	FileItem toSave = submissionValidation.getItem();
		        	
		        	submissionValidation.getDocument().setDatePost(new Timestamp(System.currentTimeMillis()));
		        	submissionValidation.getDocument().setDateLastChange(new Timestamp(System.currentTimeMillis()));
		        	submissionValidation.getDocument().setPath(UPLOAD_DIRECTORY_VIEW + File.separator + toSave.getName());
		        	submissionValidation.getDocument().setUser(userDAO.findByField("login", request.getRemoteUser()));
		        	
		        	
		        	
		        	//Insert document into database
		        	try {
						toSave.write(
								new File(UPLOAD_DIRECTORY_SAVE + File.separator + new File(toSave.getName()).getName()));
						AuthorDAO authorDAO = new AuthorDAO();
						DocumentDAO documentDAO = new DocumentDAO();
			        	for (Author a: submissionValidation.getDocument().getAuthors()) {
			        		authorDAO.update(a);
			        	}
						documentDAO.insert(submissionValidation.getDocument());
						submissionSuccess = true;
					} catch (Exception e) {
						submissionValidation.addMessage("Não foi possível gravar o arquivo. Tente novamente"+e.getMessage());
					}
				}
				else {
					submissionSuccess = uploadUpdateDocument(request);
				}
			}
		}
		
		return submissionSuccess;
	}
	
	private void setDocumentPropertiesFromRequest(HttpServletRequest request) {	
		try {
            List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                                   
            for(FileItem item : multiparts){
                if(!item.isFormField()){
                    try {
                      ContentHandler contenthandler = new BodyContentHandler(-1);
                      Metadata metadata = new Metadata();
                      metadata.set(Metadata.RESOURCE_NAME_KEY, item.getName());
                      Parser parser = new AutoDetectParser();
                      ParseContext parseContext = new ParseContext();
                      parser.parse(item.getInputStream(), contenthandler, metadata, parseContext);
                      this.submissionValidation.setFileType(metadata.get(Metadata.CONTENT_TYPE));
                      this.submissionValidation.setItem(item);
                    }
                    catch (Exception e) {
                    	this.submissionValidation.setDocument(null);
                    	this.submissionValidation.addMessage("Não foi possível ler o arquivo enviado");
                    }
                }
                else {
                	getFieldNameAndSetAttribute(item.getFieldName(), item.getString("UTF-8"), this.submissionValidation.getDocument());
                }
            }
        	for(String name: this.authorName) {
        		this.addAuthor(name);
        	}
            
        } catch (Exception ex) {
        	// Setar o documento para null impossíbilita que as informações digitadas pelo usuário sejam recarregadas 
        	// na página de submissão.
        	// ex.getmessage está retornando uma string vazia.
        	//this.submissionValidation.addMessage("Erro ao submeter o arquivo. Tente novamente"+ex.getMessage());
        	//this.submissionValidation.setDocument(null);
        }
	}
	
	private void addAuthor(String name) {
		String email = "";
		try {
			email = this.authorEmail.get(this.authorName.indexOf(name));
		} catch (Exception e) {
			email = "";
		}
		if((name != null && !name.isEmpty()) 
				&& (email != null && !email.isEmpty())) {
			Author a = new Author();
			a.setName(name);
			try {
				a.setSurname(this.authorSurname.get(this.authorName.indexOf(name)));
    		} catch (Exception e) {
    			a.setSurname("");
    		}
			a.setEmail(email);
			if (!(a.getName().isEmpty() && a.getEmail().isEmpty())) {
				this.submissionValidation.getDocument().addAuthor(a);
			}
		}
	}

	protected void getFieldNameAndSetAttribute(String fieldName, String content, Document document) {
		switch (fieldName) {
			case "doc-id":
				// The document in the request is to be updated instead
				if (!content.isEmpty()) {
					System.out.println("Content " + content);
					submissionValidation.setUpdate(true);
					docId = Long.parseLong(content);
					submissionValidation.getDocument().setId(docId);
				}
				break;
			case "title":
				document.setTitle(content);
				break;
			case "description":
				document.setDescription(content);
				break;
			case "abstract":
				document.setResume(content);
				break;
			case "authorName[]":
				this.authorName.add(content);
				break;
			case "aName":
				System.out.println(content);
				break;
			case "authorSurname[]":
				this.authorSurname.add(content);
				break;
			case "authorEmail[]":
				this.authorEmail.add(content);
				break;
			case "keyword[]":
				if (content != null && !content.isEmpty()) {
					Keyword keyword = new Keyword();
					keyword.setWord(content);
					document.addKeyword(keyword);
				}
				break;
			case "discipline[]":
				if (content != null && !content.isEmpty()) {
					Discipline discipline = new Discipline();
					discipline.setName(content);
					document.addDiscipline(discipline);
				}
				break;
			case "g-recaptcha-response":
				this.recaptchaResult = content;
				break;
			case "type":
				TypeDAO t = new TypeDAO();
				try {
					System.out.println(content);
					document.setType(t.findByField("description", content));
				} catch (Exception e) {
					e.printStackTrace();
				}
			break;
		}
	}
	private void sendEmailToAdmins(Document document, boolean newDocument) {
		final String recipient = "eds.riodosul@ifc.edu.br";
		
		// reads SMTP server setting from web.xml file
		
		UserDAO userDAO = new UserDAO();
		String subject = "Repositório Científico - " + (newDocument ? "Novo Documento Submetido" : "Documento Atualizado");
		String content = "Titulo: " + document.getTitle() + "\n\n";
		content +=  "Enviado por: " + document.getUser().getName() + "\n";
		content +=  "Data de Envio: " + document.getDatePost() + "\n";
		content +=  "Data da Última Alteração: " + document.getDateLastChange() + "\n\n";
		content += "Descrição: " + document.getDescription() + "\n";
				
		// Envia para um e-mail Fixo. Em seguida para os admins
		sendTo(recipient, content, subject);
		for (User u : userDAO.findAll()) {
			for (Role role : u.getRoles()) {
				if (role.getDescription().equalsIgnoreCase("admin")) {
					sendTo(u.getEmail(), content, subject);
				}
			}
		}
	}
	
	private void sendTo(String recipient, String content, String subject) {	
		ServletContext context = getServletContext();
		String host = context.getInitParameter("host");
		String port = context.getInitParameter("port");
		String user = context.getInitParameter("user");
		String pass = context.getInitParameter("pass");
		
		
		try {
			Email.sendEmail(host, port, user, pass, recipient, subject, content);
		} catch (Exception ex) {
			ex.printStackTrace();
			
		} finally {
			
        }
	}
}
