package br.edu.ifc.riodosul.repositoriocientifico.servlet;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifc.riodosul.repositoriocientifico.cryptography.Md5Hash;
import br.edu.ifc.riodosul.repositoriocientifico.dao.RoleDAO;
import br.edu.ifc.riodosul.repositoriocientifico.dao.UserDAO;
import br.edu.ifc.riodosul.repositoriocientifico.helper.RegisterValidation;
import br.edu.ifc.riodosul.repositoriocientifico.model.Discipline;
import br.edu.ifc.riodosul.repositoriocientifico.model.Role;
import br.edu.ifc.riodosul.repositoriocientifico.model.User;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		processRequest(request, response);

	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String to = "pages/register.jsp";
		User user = new User();
		Timestamp time = new Timestamp(System.currentTimeMillis());
		System.out.println(time.toString());
		
		if (request.getParameter("submit") != null) {
			RegisterValidation validation = new RegisterValidation();
			
			user.setName(request.getParameter("name"));
			user.setSurname(request.getParameter("surname"));
			user.setLogin(request.getParameter("username"));
			user.setPassword(request.getParameter("password"));
			user.setEmail(request.getParameter("email"));
			
			String[] disciplines = request.getParameterValues("discipline[]");
			Discipline discipline;
			for (String d: disciplines) {
				if(d != null && !d.isEmpty()) {
					discipline = new Discipline();
					discipline.setName(d);
					user.addDisciplines(discipline);
				}
			}
			user.setAccessLevel(2);
			user.setActive(false);
			user.setCreationDate(new java.sql.Timestamp(System.currentTimeMillis()));
			String confirmPassword = request.getParameter("confirm_password");
			validation.setUser(user);
			validation.setConfirmPassword(confirmPassword);
			
			if (validation.validateRecaptcha(request.getParameter("g-recaptcha-response"))) {
				
				validation.validate();
				
				if (validation.isValidReg()) {
					UserDAO userDAO = new UserDAO();
					Role role = new Role();
					RoleDAO roleDAO = new RoleDAO();
					/* Before insert the user into the database, its password is converted to MD5 */
					user.setPassword(Md5Hash.generateMd5(user.getPassword()));
					/* Remove any blank space from the e-mail */
					user.setEmail(user.getEmail().replaceAll("\\s",""));
					userDAO.insert(user);
					role.setUser(userDAO.findByField("login", user.getLogin()));
					role.setDescription("disabled_user");
					role.setLogin(user.getLogin());
					roleDAO.insert(role);
					to = "pages/auth/submission.jsp";
					request.setAttribute("registerSuccess", "Registro realizado com sucesso.");
				}
			}
			
			if(validation.getMessage().size() > 0) {
				request.setAttribute("user", user);
				request.setAttribute("failureMessages", validation.getMessage());
			}
		}
		
		if (to.contains("register")) {
			RequestDispatcher dispatcher = request
		            .getRequestDispatcher(to);
		    dispatcher.forward(request, response);
		} else {
			response.sendRedirect(request.getContextPath() + "/" + to + "?registerSuccess=true&registered="+user.getLogin());
		}
			
	}
}
