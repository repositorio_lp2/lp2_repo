package br.edu.ifc.riodosul.repositoriocientifico.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.ifc.riodosul.repositoriocientifico.dao.TypeDAO;
import br.edu.ifc.riodosul.repositoriocientifico.model.Type;

/**
 * Servlet implementation class TypeManagementServlet
 */
@WebServlet("/TypeManagementServlet")
@ServletSecurity(
        value = @HttpConstraint(
                rolesAllowed = {
                        "admin"
                }),
        httpMethodConstraints = {
                @HttpMethodConstraint(value = "GET", rolesAllowed = {
                		"admin"
                }),
                @HttpMethodConstraint(value = "POST", rolesAllowed = {
                		"admin"
                })
        })
public class TypeManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TypeManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String to = "pages/auth/admin/category-management.jsp";
		TypeDAO typeDAO = new TypeDAO();
		
		if (request.getParameter("new-type") != null && !request.getParameter("new-type").isEmpty()) {
			if (typeDAO.findByField("description", request.getParameter("new-type")) == null) {
				Type type = new Type();
				type.setDescription(request.getParameter("new-type"));
				typeDAO.insert(type);
				request.setAttribute("sucessMessage", "A descrição da categoria foi inserida com sucesso.");
			}
			else {
				request.setAttribute("failureMessage", "A descrição da categoria já existe na base de dados.");
			}
		}
		
		else if (request.getParameter("type-to-remove") != null && !request.getParameter("type-to-remove").isEmpty()) {
			System.out.println(request.getParameter("type-id"));
			if (request.getParameter("type-id") != null && !request.getParameter("type-id").isEmpty()) {
				String typeId = request.getParameter("type-id");
				boolean removed = typeDAO.delete(typeDAO.findById(Long.parseLong(typeId)));

				if (removed) {
					request.setAttribute("sucessMessage", "A descrição foi removida com sucesso.");
				}
				else {
					request.setAttribute("failureMessage", "A descrição não pode ser removida.");
				}
			}
		}

		List<Type> types = typeDAO.findAll();
		request.setAttribute("types", types);
		
		RequestDispatcher dispatcher = request
	            .getRequestDispatcher(to);
	    dispatcher.forward(request, response);	
	}
}
