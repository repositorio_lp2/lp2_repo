package br.edu.ifc.riodosul.repositoriocientifico.cryptography;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Hash {
	/* This class has methods whose generates MD5 Hash for a password string */
	public static String generateMd5(String password) {
		
		String md5 = null;
		
		if(password == null) { 
			return null;
		}
		try {
			
		/* Create MessageDigest object for MD5 */
		MessageDigest digest = MessageDigest.getInstance("MD5");

		/* Update input string in message digest */
		digest.update(password.getBytes(), 0, password.length());
		md5 = new BigInteger(1, digest.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return md5;
	}

	/* Checks Whether a password hash matches with the one from the database */
	public static boolean verifyPasswordMd5(String password, String md5Hash) {
		return generateMd5(password).equals(md5Hash);
	}
}
