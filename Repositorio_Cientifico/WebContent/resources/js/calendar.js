	$(document).ready(function() {
		
		$('#datePicker').datepicker({
			format : 'mm/dd/yyyy'
		}).on('changeDate', function(e) {
			// Revalidate the date field
			$('#eventForm').formValidation('revalidateField', 'date');
		});
	
		$('#eventForm').formValidation({
			framework : 'bootstrap',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				name : {
					validators : {
						notEmpty : {
							message : 'The name is required'
						}
					}
				},
				date : {
					validators : {
						notEmpty : {
							message : 'The date is required'
						},
						date : {
							format : 'MM/DD/YYYY',
							message : 'The date is not a valid'
						}
					}
				}
			}
		});
	});	
	
	$(document).ready(function() {
		$('#datePicker1').datepicker({
			format : 'mm/dd/yyyy'
		}).on('changeDate', function(e) {
			// Revalidate the date field
			$('#eventForm').formValidation('revalidateField', 'date');
		});
	
		$('#eventForm').formValidation({
			framework : 'bootstrap',
			icon : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				name : {
					validators : {
						notEmpty : {
							message : 'The name is required'
						}
					}
				},
				date : {
					validators : {
						notEmpty : {
							message : 'The date is required'
						},
						date : {
							format : 'MM/DD/YYYY',
							message : 'The date is not a valid'
						}
					}
				}
			}
		});
	});	
	
	/*
	//We then add validator rules for the selectedDate field, don't forget to set excluded: 
	//false to the field (because it's hidden so it will be ignored by default):
	$('#eventForm').formValidation({
	    framework: 'bootstrap',
	    icon: {
	        ...
	    },
	    fields: {
	        selectedDate: {
	            // The hidden input will not be ignored
	            excluded: false,
	            validators: {
	                notEmpty: {
	                    message: 'The date is required'
	                },
	                date: {
	                    format: 'MM/DD/YYYY',
	                    message: 'The date is not a valid'
	                }
	            }
	        }
	    }
	});
	
	//Finally, after choosing a date, you need to set the 
	//selected date to the hidden field, and revalidate it:
	('#embeddingDatePicker')
	    .datepicker({
	        format: 'mm/dd/yyyy'
	    })
	    .on('changeDate', function(e) {
	        // Set the value for the date input
	        $("#selectedDate").val($("#embeddingDatePicker").datepicker('getFormattedDate'));
	
	        // Revalidate it
	        $('#eventForm').formValidation('revalidateField', 'selectedDate');
	});
	*/