$(document).ready(function(){
	
//    window.countElementsIn = function(containerSel, patternSel) {
//        var parentE = $(containerSel);
//        var parentCnt = parentE.length;
//        if(parentCnt > 0) {
//            return parentE.find(patternSel);
//        }
//        return 0;
//    } 
//    
//    window.genInput = function(baseName,type,val,fieldType,extraClass) {
//        if (fieldType == 1){
//        	return '<label class="label-pr">Autor</label>'+
//					'<input autocomplete="off" id="#author" class="input" name="authorName[]" type="text" placeholder="Nome" data-items="8" value="'+val.prev().prev().prev().val()+'" required="true"/>'+
//					'<input autocomplete="off" id="#author" class="input" name="authorSurname[]" type="text" placeholder="Sobrenome" data-items="8" value="'+val.prev().prev().val()+'"/>'+
//					'<input autocomplete="off" id="#author" class="input" name="authorEmail[]" type="text" placeholder="E-mail" data-items="8" value="'+val.prev().val()+'" required="true"/>'
//        } else if (fieldType == 3) {
//        	return '<label class="label-pr">Disciplina</label><input class="input group-control '+extraClass+'" name="'+baseName+'[]" type="'+type+'" value="'+val+'">';
//        } else {
//        	return '<label class="label-pr">Palavra-chave</label><input class="input group-control '+extraClass+'" name="'+baseName+'[]" type="'+type+'" value="'+val+'">';
//        }
//    }
//    
//    window.genBtn = function(label,extraClass) {
//        return '<button id="b3" class="btn '+extraClass+' add" type="button">'+label+'</button>';
//    }
//    
//    window.genNewRow = function(type,val,fieldType,baseName) {
//        switch(type) {
//            default: 
//                return '<div class="span12 mbottom">'+genInput(baseName,'text',val,fieldType)+' '+genBtn('-', 'btn-danger remove-parent')+'</div>';
//        }
//        console.log('Check your code Im broken...')
//        return ''; // should never happen
//    }
//    $("#b1").on('click', function(e){
//        e.preventDefault(); // prevents button weird behaviour
//        var me = $(this);
//        var addTo = me.data('append-to') != undefined ? me.data('append-to') : "#author_container";
//        var newType = me.data('append-type') != undefined ? me.data('append-type') : 'text'; // text is just default
//        var newRec = genNewRow( newType, me, 1);
//        $(addTo).append( newRec );
//        me.prev().val(''); // wipe our value
//        me.prev().prev().val('');
//        me.prev().prev().prev().val('');
//
//        $('.remove-parent').click(function(){
//            e.preventDefault(); // prevents button weird behaviour
//            $(this).parent().remove();
//        });
//    });
//    $("#b2").on('click', function(e){
//        e.preventDefault(); // prevents button weird behaviour
//        var me = $(this);
//        var addTo = me.data('append-to') != undefined ? me.data('append-to') : "#keyword_container";
//        var newType = me.data('append-type') != undefined ? me.data('append-type') : 'text'; // text is just default
//        var newRec = genNewRow( newType, me.prev().val(), 2, 'keyword');
//        $(addTo).append( newRec );
//        me.prev().val(''); // wipe our value
//        
//        $('.remove-parent').click(function(){
//            e.preventDefault(); // prevents button weird behaviour
//            $(this).parent().remove();
//        });
//    });
//    $("#b3").on('click', function(e){
//        e.preventDefault(); // prevents button weird behaviour
//        var me = $(this);
//        var addTo = me.data('append-to') != undefined ? me.data('append-to') : "#discipline_container";
//        var newType = me.data('append-type') != undefined ? me.data('append-type') : 'text'; // text is just default
//        var newRec = genNewRow( newType, me.prev().val(), 3, 'discipline');
//        $(addTo).append( newRec );
//        me.prev().val(''); // wipe our value
//        
////        console.log(document.getElementsByName("discipline[]"));
//        
//        
//        $('.remove-parent').click(function(){
//            e.preventDefault(); // prevents button weird behaviour
//            $(this).parent().remove();
//        });
//    });

	isReq();
});


function addDiscipline(btn) {
	
	let newDiscipline = '<div class="span12 mbottom">'+
							'<label class="label-pr">Disciplina</label>'+
							'<input class="input group-control right-margin" name="discipline[]" type="text" value="'+$(btn).prev().val()+'">'+
							'<button class="btn btn-danger" type="button" onClick="removeElement(this)">-</button>'+
						'</div>';
	
	$(btn).prev().val('');
	document.getElementById('discipline_container').innerHTML += newDiscipline;
}

function addAuthor(btn) {
	 
	let newAuthor = '<div class="span12 mbottom">'+
						'<label class="label-pr">Autor</label>'+
						'<input class="input" name="authorName[]" type="text" placeholder="Nome" data-items="8" value="'+$(btn).prev().prev().prev().val()+'" required="true"/>'+
						'<input class="input" name="authorSurname[]" type="text" placeholder="Sobrenome" data-items="8" value="'+$(btn).prev().prev().val()+'"/>'+
						'<input class="input right-margin" name="authorEmail[]" type="email" placeholder="E-mail" data-items="8" value="'+$(btn).prev().val()+'" required="true"/>'+
						'<button class="btn btn-danger" onClick="removeElement(this)" type="button">-</button>'+
					'</div>';
	
	$(btn).prev().val('');
	$(btn).prev().prev().val('');
	$(btn).prev().prev().prev().val('');
	document.getElementById('author_container').innerHTML += newAuthor;
	
	isReq();
}

function addKeyword(btn) {
	 
	let newKeyword = '<div class="span12 mbottom">'+
							'<label class="label-pr">Palavra-chave</label>'+
							'<input class="input group-control right-margin" name="keyword[]" type="text" value="'+$(btn).prev().val()+'">'+
							'<button class="btn btn-danger" type="button" onClick="removeElement(this)">-</button>'
						'</div>'
	
	$(btn).prev().val('');
	document.getElementById('keyword_container').innerHTML += newKeyword;
}

function removeElement(btn) {	
	$(btn).parent().remove();
	isReq();
}

function isReq() {
	let authors = document.getElementsByName("authorName[]");
	console.log(authors.length)
	
	if(authors.length == 1){
		document.getElementById("aName").required = true;
		document.getElementById("aEmail").required = true;
	} else {
		document.getElementById("aName").required = false;
		document.getElementById("aEmail").required = false;
	}
}

