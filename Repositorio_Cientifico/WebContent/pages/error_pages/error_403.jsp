<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">
	<jsp:include page="../head.jsp" flush="true"/>
		
<style>
	.body{
    font-family: 'Ropa Sans', sans-serif;
    margin-top: 30px;
    background-color: #F0CA00;
    background-color: #D3D3D3;
    text-align: center;
    color: #fff;
}
.error-heading{
    margin: 50px auto;
    width: 250px;
    border: 5px solid #fff;
    font-size: 126px;
    line-height: 126px;
    border-radius: 30px;
    text-shadow: 6px 6px 5px #000;
}
.error-heading img{
    width: 100%;
}
.error-main h1{
    font-size: 72px;
    margin: 0px;
    color: #F3661C;
    text-shadow: 0px 0px 5px #fff;
}
	
</style>
</head>
<body>
	<jsp:include page="../menu-hamburger.jsp" flush="true"/>
	
	<div class="error-main">
		<h1 style="text-align:center">Oops!</h1>
		<div class="error-heading body">403</div>
		<div style="text-align:center">
		<p><em>Você não tem autorização para acessar essa página. Entre em contato com o administrador.
		</em></p>
		<p><a href="../contact.jsp">Contato</a></p>
		<p><a href="../main-page.jsp">Página inicial</a></p>
		
		</div>
	</div>
</body>
</html>


