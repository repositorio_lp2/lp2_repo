<%@page import="java.security.Principal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../head.jsp" flush="true"/>
		
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/dynamic-form-field.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/submission.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/captcha.css"></link>
		<script type="text/javascript" src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/dynamic-form-field.js"></script>
		
		<!-- Captcha Google -->
		<script src='https://www.google.com/recaptcha/api.js'></script>
		
		<title>Submissão</title>
	</head>
	<body>
		<!-- https://bootsnipp.com/snippets/90Gom -->
		<!-- Import menu-hamburger -->
		<jsp:include page="../menu-hamburger.jsp" flush="true"/>
	
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<!-- Form Register -->
					<form class="form-horizontal" method="post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/SubmissionServlet">						
						<div class="text-center mbottom">
							<h1>Submissão</h1>
						</div>
						<c:if test="${submissionSuccess != null}">
							<div class="alert alert-success alert-dismissable fade in">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
								<strong>Sucesso:</strong> ${submissionSuccess}
							</div>
						</c:if>
						<c:if test="${failureMessages!=null}">
							<c:forEach var="m" items="${failureMessages}">
								<div class="alert alert-danger alert-dismissable fade in">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
									<strong>Erro:</strong> ${m}
								 </div>
							</c:forEach>
						</c:if>
						<div class="form-group">
							<p>Campos com <b class="required-field">*</b> são obrigatórios</p>
						</div>
						
						<!-- Title input-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Mínimo 3, máximo 512 caracteres</span>
							<input id="title" name="title" type="text" placeholder="Título" class="form-control input-md" value="${document.title}" required>
						</div>
						
						<!-- Description Text area -->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Mínimo 10, máximo 255 caracteres</span>
							<textarea class="form-control" name="description" id="description" placeholder="Descrição" maxlength="255" rows="7">${document.description}</textarea>
						</div>
						
						<!-- Abstract Text area-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Mínimo 50, máximo 4096 caracteres</span>
							<textarea class="form-control" name="abstract" id="abstract" placeholder="Resumo" maxlength="4096" rows="7">${document.resume}</textarea>
						</div>
						
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Selecione uma categoria</span>
							<select class="form-control" title="Categoria Documento" id="document-category" name="type">
								<option selected disabled>Categoria</option>
								<c:forEach var="t" items="${types}">
									<option ${document.type.description == t.description ? 'selected' : ''}>${t.description}</option>
								</c:forEach>
								<!--  <option>OUTRO</option>  -->
							</select>
						</div>
						
						<div class="row form-group">
							<div class="control-group" id="fields">
								<div class="controls" id="profs">
									<span class="value-validation">Adicione disciplinas que se apliquem ao documento</span>
									<div class="row-fluid">
										<div id="discipline_container">
											<c:forEach var="d" items="${document.disciplines}">
												<div class="span12 mbottom">
													<label class="label-pr">Disciplina</label>
													<input class="input group-control " name="discipline[]" type="text" value="${d.name}">
													<button class="btn btn-danger" onClick="removeElement(this)" type="button">-</button>
												</div>
											</c:forEach>
											<!-- WHERE THE DYNAMIC DISCIPLINE INPUTS ARE GOING -->
										</div>
									</div>
									<div class="row-fluid control-group mbottom">
										<label class="label-pr">Disciplina:</label>
										<input autocomplete="off" class="input" id="keyword" name="discipline[]" type="text" placeholder="Disciplina" data-items="8"/>
										<button class="btn add" onClick="addDiscipline(this)" type="button">+</button>
									</div>
									<b class="required-field">*</b><span class="value-validation">Adicone ao menos um autor. Nome e E-mail são obrigatórios</span>
									<div class="row-fluid">
										<div id="author_container" class="">
											<c:forEach var="d" items="${document.authors}">
												<div class="span12 mbottom">
													<label class="label-pr">Autor</label>
													<input class="input" name="authorName[]" type="text" placeholder="Nome" data-items="8" value="${d.name}" required="true"/>
													<input class="input" name="authorSurname[]" type="text" placeholder="Sobrenome" data-items="8" value="${d.surname}"/>
													<input class="input" name="authorEmail[]" type="email" placeholder="E-mail" data-items="8" value="${d.email}" required="true"/>
													<button class="btn btn-danger" onClick="removeElement(this)" type="button">-</button>
												</div>
											</c:forEach>
											<!-- WHERE THE DYNAMIC AUTHOR INPUTS ARE GOING -->
										</div>
									</div>
									<div class="row-fluid control-group mbottom">
										<label class="label-pr">Autor:</label>
										<input autocomplete="off" class="input" id="aName" name="authorName[]" type="text" placeholder="Nome" data-items="8" required/>
										<input autocomplete="off" class="input" id="aSurname" name="authorSurname[]" type="text" placeholder="Sobrenome" data-items="8"/>
										<input autocomplete="off" class="input" id="aEmail" name="authorEmail[]" type="text" placeholder="E-mail" data-items="8" required/>
										<button onClick="addAuthor(this)" class="btn add" type="button">+</button>
									</div>
									
									<span class="value-validation">Adicione palavras-chave do documento ou que possam auxiliar nas pesquisas</span>
									<div class="row-fluid">
										<div id="keyword_container">
											<c:forEach var="d" items="${document.keywords}">
												<div class="span12 mbottom">
													<label class="label-pr">Palavra-chave</label>
													<input class="input group-control " name="keyword[]" type="text" value="${d.word}">
													<button class="btn btn-danger" onClick="removeElement(this)" type="button">-</button>
												</div>
											</c:forEach>
											<!-- WHERE THE DYNAMIC KEYWORDS INPUTS ARE GOING -->
										</div>
									</div>
									<div class="row-fluid control-group mbottom">
										<label class="label-pr">Palavra-chave: </label>
										<input autocomplete="off" class="input" id="keyword" name="keyword[]" type="text" placeholder="Palavra Chave" data-items="8"/>
										<button onClick="addKeyword(this)" class="btn add" type="button">+</button>
									</div>					
								</div>
							</div>
						</div>
						
						<!-- File Picker -->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> É obrigatório o envio de um arquivo PDF</span>
							<input type="file" name="file" size="50" class="mbottom" />
						</div>
						
						
						<!-- Captcha Google -->
						<div class="text-xs-center form-group">
							<div class="g-recaptcha" data-sitekey="6LfqnjgUAAAAAKmpvdi-YKqyf9mtUgBMtw_-dD6d">
								<!-- CAPTCHA -->
							</div>
						</div>
						
						<!-- Button -->
						<div class="form-group text-center send-div">
							<input id="submit" name="submit" type="submit" class="btn send" value="Enviar">
							<input type ="hidden" name="doc-id" value="${document.id}"/>						
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>