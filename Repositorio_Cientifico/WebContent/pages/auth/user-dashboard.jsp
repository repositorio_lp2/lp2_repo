<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
 
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/user-dashboard.css"></link>
		
		<title>Administrador</title>
	</head>
	<body>
		
		<!-- Import menu-hamburger -->
		<jsp:include page="../menu-hamburger.jsp" flush="true"/>
		<c:if test="${registerUpdateSuccess != null}">
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
				<strong>Sucesso:</strong> ${registerUpdateSuccess}
			</div>
		</c:if>
		<% request.setAttribute("isAdmin", request.isUserInRole("admin")); %>
		<div class="container col-md-12">
			<div class="row">
				<div class="text-center">	
					<h1>Home</h1>
					<h1>Administrador</h1>
				</div>
				 <form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/DashboardServlet">
				    <div class="row space-top">
				        <div class="col-md-4 brd col-md-offset-2">
				            <div class="col-md-12">
								<input id="submission" name="submission" type="submit" class="btn btn-lg btn-block" value="Nova Submissão">
						    </div>
				        </div>
				        <div class="col-md-4 brd">
				            <div class="col-md-12">
								<input id="submission-management" name="submission-management" type="submit" class="btn btn-lg btn-block" value="Gerência de Submissões">
							</div>
				        </div>
				        <div class="row space-top-row instructors-button">
					        <div class="col-md-4 brd col-md-offset-2">
					            <div class="col-md-12 center-block">
									<input id="update-register" name="update-register" type="submit" class="btn btn-lg btn-block " value="Atualizar Cadastro">
								</div>
					        </div>
					        <c:if test="${requestScope.isAdmin}">
						    	<div class="col-md-4 brd">
						            <div class="col-md-12">
										<input id="teaching-management" name="teaching-management" type="submit" class="btn btn-lg btn-block " value="Gerência de Docentes">
									</div>
						        </div>
						    </c:if>
					    </div>
					     <div class="row space-top-row-2 instructors-button">
					    	<c:if test="${requestScope.isAdmin}">
						    	<div class="col-md-4 brd col-md-offset-6">
						            <div class="col-md-12">
										<input id="category-management" name="category-management" type="submit" class="btn btn-lg btn-block " value="Gerência de Categorias">
									</div>
							   </div>
							</c:if>
						</div>
				    </div>
				</form>
			</div>
		</div>
	</body>
</html>