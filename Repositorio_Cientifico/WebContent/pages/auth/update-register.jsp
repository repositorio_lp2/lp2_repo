<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/register.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/dynamic-form-field.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/captcha.css"></link>
		
		<script type="text/javascript" src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/update-register.js"></script>
			
		<!-- Captcha Google -->
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<title>Atualizar Registro</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="../menu-hamburger.jsp" flush="true"/>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/RegisterUpdateServlet">
						<!-- Form Register -->
						<div class="text-center mbottom">
							<h1>Atualizar Registro</h1>
						</div>
						<c:if test="${user!=null}">
							<c:if test="${failureMessages!=null}">
								<c:forEach var="m" items="${failureMessages}">
								<div class="alert alert-danger alert-dismissable fade in">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
										<strong>Erro:</strong> ${m}
								 </div>
								</c:forEach>
							</c:if>
						</c:if>
						
						<div class="form-group">
							<p>Campos com <b class="required-field">*</b> são obrigatórios</p>
						</div>
						
						<!-- First Name input-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Mínimo 3, máximo 45 caracteres</span> 
							<input id="name" name="name" type="text" placeholder="Nome" value="${user.name}" class="form-control input-md" required/>
						</div>
				
						<!-- Last Name input-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Mínimo 3, máximo 45 caracteres</span>
							<input id="surname" name="surname" type="text" placeholder="Sobrenome" value="${user.surname}" class="form-control input-md"/>
						</div>
				
						<!-- User Name input-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation"> Sem caracteres especiais e espaços, começar com letras e ter 6 caracteres (Apenas o administrador pode mudar)</span>
							<input id="username" name="username" type="text" placeholder="Usuário" value="${user.login}" class="form-control input-md" required readonly/>
						</div>
						
							<!-- Old  Password input-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation">Digite sua senha atual</span>
							<input id="password" name="current_password" type="password" placeholder="Senha atual" class="form-control input-md" required/>
						</div>

						<div class="form-group">
							<input id="checkbox" name="change-password" type="checkbox" onchange="setPasswordField();"/>
							<label class="value-validation" for="checkbox">Alterar senha</label>
						</div>
				
						<!-- Password input-->
						<div class="form-group" id="changepassword" hidden="true">
							<b class="required-field">*</b><span class="value-validation">Mínimo 8 caracteres com letra maiúscula, minúscula, caractere especial e número</span>
							<input id="password" name="password" type="password" placeholder="Senha" class="form-control input-md"/>
						</div>
				
						<!-- Confirm Password input-->
						<div class="form-group" id="changeconfirm" hidden="true">
							<b class="required-field">*</b>
							<input id="confirm_password" name="confirm_password" type="password" placeholder="Confirmar senha" class="form-control input-md"/>
						</div>
				
						<!-- Email input-->
						<div class="form-group">
							<b class="required-field">*</b><span class="value-validation">Deve ser um e-mail válido</span>
							<input id="email" name="email" type="email" placeholder="E-mail" value="${user.email}" class="form-control input-md"/>
						</div>
						
						<div class="row form-group">
							<span class="value-validation">Adicione disciplinas que leciona</span>
							<div class="control-group" id="fields">
								<div class="controls" id="profs">
									<div class="row-fluid">
										<div id="discipline_container">
											<c:forEach var="d" items="${user.disciplines}">
												<div class="span12 mbottom">
													<label class="label-pr">Disciplina</label>
													<input class="input group-control " name="discipline[]" type="text" value="${d.name}">
													<button class="btn btn-danger" onClick="removeElement(this)" type="button">-</button>
												</div>
											</c:forEach>
											<!-- WHERE THE DYNAMIC DISCIPLINE INPUTS ARE GOING -->
										</div>
									</div>
									<div class="row-fluid control-group mbottom">
										<label class="label-pr">Disciplina:</label>
										<input autocomplete="off" class="input" id="discipline" name="discipline[]" type="text" placeholder="Disciplina" data-items="8"/>
										<button class="btn add" onClick="addDiscipline(this)" type="button">+</button>
									</div>				
								</div>
							</div>
						</div>
						
						<!-- Captcha Google -->
						<div class="text-xs-center form-group">
							<div class="g-recaptcha" data-sitekey="6LfqnjgUAAAAAKmpvdi-YKqyf9mtUgBMtw_-dD6d"></div>
						</div>
			
						<!-- Button -->
						<div class="form-group text-center mtop">
						    <input type ="hidden" name="user-id" value="${user.id}"/>
							<input id="submit" name="submit-update-register" type="submit" class="btn send" value="Enviar">
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>