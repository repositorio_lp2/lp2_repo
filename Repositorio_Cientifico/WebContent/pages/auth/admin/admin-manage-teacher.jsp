<%@page import="java.sql.Timestamp"%>
<%@page import="br.edu.ifc.riodosul.repositoriocientifico.model.Document"%>
<%@page import="br.edu.ifc.riodosul.repositoriocientifico.dao.DocumentDAO"%>
<%@page import="br.edu.ifc.riodosul.repositoriocientifico.model.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../../head.jsp" flush="true" />
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/register.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/captcha.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/admin-manage-teacher.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap-toggle.min.css"></link>
		<!-- Imports permanents scripts -->
		
		<title>Gerência Docentes</title>
	</head>
<body>
	<!-- Import menu-hamburger -->
	<jsp:include page="../../menu-hamburger.jsp" flush="true" />
	<!--  -->
	<div class="container col-md-12">
		<div class="row" style="margin: 10px 20px;">
			<div class="form-group col-sm-4">
				<h2>Gerência de Usuários</h2>
				<h4>___________________</h4>
			</div>
		<!-- <div class="" align="right">
				<div>
					<img src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/images/logoifc.png" 
					class="" alt="" style="width: 8%" />
				</div>
			</div> -->
			<c:if test="${users!=null}">
			
				<c:forEach var="user" items="${users}">
					<div class="col-xs-10 col-md-12 thumbnail line-break">
						<form name="form" action="/Repositorio_Cientifico/DashboardServlet" method="POST">
							<div class="section-content">
								<div class="col-md-6">
									<h3 class="section-header">Nome: ${user.name} ${user.surname}</h3>
								</div>
								<c:choose>
									<c:when test="${user.active}">
										<div class="col-md-6 text-right">
											<input id="disable-user" name="disable-user" type="submit" class="btn btn-danger" value="desativar usuário">
											<!--<input  type="checkbox" name="disable-user"  data-toggle="toggle" data-style="ios" data-on="Ativo" data-off="Inativo" value="${user.id}" checked onChange="javascript:document.form.submit();" > -->
											<!-- <input id="enable-disable-user" name="${user.active ? 'disable-user' : 'enable-user'} type="submit" class="btn" value="${user.active ? 'desativar' : 'ativar'}"> -->
										</div>
									</c:when>
									<c:when test="${not user.active}">
										<div class="col-md-6 text-right">
											<input id="enable-user" name="enable-user" type="submit" class="btn btn-primary" value="ativar usuário">
											<!--<input type="checkbox" name="enable-user"  data-toggle="toggle" data-style="ios" data-on="Ativo" data-off="Inativo" value="${user.id}" onChange="javascript:document.form.submit();"> -->
										</div>
									</c:when>
								</c:choose>
							</div>
							<div class="contact-section">
								<div class="col-md-12 well well-sm">
									<div class="col-lg-2 thumbnail">
										<!-- YEAR -->
										<p>${user.email}</p>
									</div>
									<div class="col-lg-3 thumbnail">
										<!--  -->
										<p>${fn:length(user.documents)} documentos submetidos</p>
									</div>
									<div class="col-lg-4 thumbnail">
										<!--  -->
										<%
										DocumentDAO documentDAO = new DocumentDAO();
										User u = (User) pageContext.getAttribute("user");
										documentDAO.findByLastDateAndUserId(u.getId());
										Timestamp lastDatePost  = documentDAO.findByLastDateAndUserId(u.getId());
										
										if (lastDatePost != null) {
											request.setAttribute("lastDate", lastDatePost);
										}
										%>
										<p>Ultima submissão: <c:choose><c:when test="${lastDate == null}"> Nenhuma submissão realizada</c:when> <c:otherwise>${lastDate}</c:otherwise></c:choose></p>
									</div>
									<input type="hidden" id="user-id" name="user-id" value="${user.id}" />
									<div class="text-right space-top">
										<input id="edit-user-submission" name="edit-user-submission" type="submit" class="btn" value="Editar Submissões">
										<!-- BUTTON FOR DOCUMENT DONWLOAD -->
										<input id="delete-user" name="delete-user" type="submit" class="btn btn-danger" value="Excluir usuário">
										<!-- BUTTON FOR CODUMENT DELETE -->
									</div>
								</div>
							</div>
						</form>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</div>
	<script src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/bootstrap-toggle.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<!-- Captcha Google -->
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>