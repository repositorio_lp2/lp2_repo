<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../../head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/register.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/captcha.css"></link>
		<!-- Captcha Google -->
		<script src='https://www.google.com/recaptcha/api.js'></script>
		
		<title>Gerência de Cadastro de Docente</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="../../menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/RegisterServlet">
						<!-- Form Register -->
						<div class="text-center">
							<h1>Gerência de Cadastro de Docente</h1></br>
							
						</div>						
						<c:if test="${user!=null}">
							<c:if test="${failureMessages!=null}">
								<c:forEach var="m" items="${failureMessages}">
								<div class="alert alert-danger alert-dismissable fade in">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
										<strong>Erro:</strong> ${m}
								 </div>
								</c:forEach>
							</c:if>
						</c:if>	
							
						<!-- First Name input-->
						<div class="form-group">
							<input id="name" name="name" type="text" placeholder="Nome" value="${user.name}" class="form-control input-md" required="">
						</div>
				
						<!-- Last Name input-->
						<div class="form-group">
							<input id="surname" name="surname" type="text" placeholder="Sobrenome" value="${user.surname}" class="form-control input-md" required="">
						</div>
				
						<!-- User Name input-->
						<div class="form-group">
							<input id="username" name="username" type="text" placeholder="Usuário" value="${user.login}" class="form-control input-md" required="">
						</div>
				
						<!-- Password input-->
						<div class="form-group">
							<input id="password" name="password" type="password" placeholder="Senha" class="form-control input-md" required="">
						</div>
				
						<!-- Confirm Password input-->
						<div class="form-group">
							<input id="confirm_password" name="confirm_password" type="password" placeholder="Confirmar senha" class="form-control input-md" required="">
						</div>
				
						<!-- Email input-->
						<div class="form-group">
								<input id="email" name="email" type="email" placeholder="E-mail" value="${user.email}" class="form-control input-md">
						</div>

						<!-- Captcha Google -->
						<div class="text-xs-center form-group">
							<div class="g-recaptcha" data-sitekey="6LfqnjgUAAAAAKmpvdi-YKqyf9mtUgBMtw_-dD6d"></div>
						</div>
			
						<!-- Button -->
						<div class="form-group text-center mtop">
							<input id="submit" name="submit" type="submit" class="btn send" value="Enviar">
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>