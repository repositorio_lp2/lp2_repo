<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="../../head.jsp" flush="true" />
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap-toggle.min.css"></link>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
		<!-- Imports permanents scripts -->
		<!-- Got from -->
<title>Gerência de Categorias</title>

</head>

<body>
	<jsp:include page="../../menu-hamburger.jsp" flush="true"/>
	
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
			<c:if test="${failureMessage!=null}">
				<div class="alert alert-danger alert-dismissable fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
					<strong>Erro:</strong> ${failureMessage}
				</div>
			</c:if>
			<c:if test="${sucessMessage!=null}">
				<div class="alert alert-success alert-dismissable fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
					<strong>Sucesso:</strong> ${sucessMessage}
				</div>
			</c:if>
        	<label class="control-label"  for="aluno">Categorias</label>
        		<div class="controls">
        		
        		<form name="form" action="/Repositorio_Cientifico/TypeManagementServlet" method="POST">
        		
            		<input type="text" value="" list="type-to-remove" name="type-to-remove" id="type-selected" >
           			<input type="hidden" id="type-id" name="type-id">
              		<input id="remove-type" name="remove-type" type="submit" class="btn btn-danger" value="Remover">
              		<!--   <button class="btn btn-danger remove" type="button" ><i class="glyphicon glyphicon-remove float-right"></i> Remover</button> -->
            	<datalist id="type-to-remove">
				<c:forEach var="type" items="${types}">
	            	<option id="${type.id}" value="${type.description}" data-id="${type.id}"></option>
				</c:forEach>
            	</datalist>
            	</form>
        	</div>
        	<form name="form" action="/Repositorio_Cientifico/TypeManagementServlet" method="POST">
        	<input autocomplete="off" class="input" id="new-type" name="new-type" type="text" placeholder="Categoria"/>
        	<input id="add-type" name="add-type" type="submit" class="btn btn-primary" value="Adicionar	">
			 <!-- <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Adicionar</button> !-->	
		</form>
          </div>
        </div>
	</div>

   	<script>
        $(document).ready(function() {
 
            $("#type-selected").on("change", function(){
                el = "#type-to-remove [value='" + $(this).val()+"']";
                $("#type-id").val($(el).data("id"));
        });
    });
          </script>
</body>

</html>