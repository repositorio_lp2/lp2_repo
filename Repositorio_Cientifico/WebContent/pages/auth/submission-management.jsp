<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/admin-manage-document.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/search.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/font-awesome.min.css">
		
		<title>Gerência de Submissão</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="../menu-hamburger.jsp" flush="true"/>
		<!--  -->

			

		<div class="form-group col-sm-4">	
			<h2>Gerência de Submissão</h2>
			<h4>___________________</h4>
			<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/SubmissionServlet">
				<input id="submission" name="submission" type="submit" class="btn btn-default" value="Nova Submissão">
			</form>
		</div>
		<c:if test="${documents!=null}">
			<c:forEach var="doc" items="${documents}">
				<div class="container col-xs-10 col-md-12 thumbnail line-break">
					<div class="section-content">
						<h3 class="section-header">Título: ${doc.title}</h3>
					</div>
					<div class="contact-section">
				  		<div class="col-md-12 well well-sm"><h5>Descrição:</h5>
				  			<div class="form-group thumbnail">
								<p>
								${doc.description}
								</p>
							</div>
					  		<div class="col-lg-2 thumbnail"> <!-- YEAR -->
					  			<p>
					  				Postado em: ${doc.datePost}
					  			</p>
							</div>
							<div class="col-lg-3 thumbnail"> <!-- Author -->
					  			<p>Autores: 
					  				<c:choose>
						  					<c:when test="${doc.authors!=null}">
												<c:forEach var="authors" items="${doc.authors}">
													${authors.name},
												</c:forEach> 
											</c:when>
										<c:otherwise>
											Autores chave não cadastradas.
										</c:otherwise>
									</c:choose>
					  			</p>
						 		</div>
						    <div class="col-lg-4 thumbnail"> <!-- Keywords -->
					  			<p>Palavras-chave: 
					  				<c:choose>
						  					<c:when test="${doc.keywords!=null}">
												<c:forEach var="keywords" items="${doc.keywords}">
													${keywords.word},
												</c:forEach> 
											</c:when>
										<c:otherwise>
											Palavras chave não cadastradas.
										</c:otherwise>
									</c:choose>
					  			</p>
							</div>
							<form method="post" action="/Repositorio_Cientifico/DashboardServlet">
								<div class="col-sm-2">
									<div class="center download">
						  				<a href=${doc.path} class="btn btn-default" download="${doc.path}">Download</a> <!-- LINK PARA DOWNLOAD DO DOCUMENTO -->
						  			</div>
					  					<div class="center"> 			  					
					  						<input type ="hidden" name="doc-id" value="${doc.id}" />
					  					</div>
					  					<div class="center">
					  						<input type ="submit" class="btn btn-secundary" name="display-document" value="Visualizar" />
				  						</div>
					  			</div>
					  			<div class="row">
									<div class="form-group">
						  				<input id="edit" name="edit" type="submit" class="btn"
												value="Editar"> <!-- BUTTON FOR DOCUMENT DONWLOAD -->
									</div>		  					
					  				<div class="form-group">
						  				<input id="delete" name="delete" type="submit" class="btn btn-danger"
												value="Excluir"> <!-- BUTTON FOR CODUMENT DELETE -->
				  					</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</c:forEach>
		</c:if>
	</body>
</html>