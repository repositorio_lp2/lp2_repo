<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="../head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/user-dashboard.css"></link>
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
		
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
		<title>Gerência de Docente</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="../menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<div class="container col-md-12">
			<div class="form-group col-sm-6">		
				<h2>Gerência de Docente</h2>
				<h4>___________________</h4></br>
			</div>
<%-- 			<c:if test="${author!=null}"> --%>
<%-- 				<c:forEach var="author" items="${author}"> --%>
					<div class="container col-sm-10 col-xs-10 col-md-12 thumbnail line-break">
						<div class="section-content">
							<h3 class="section-headercol-sm-9 col-xs-9 col-md-11"> JUVENAU<%-- ${author.name} ${author.surname}--%></h3>
							 <div class="col-sm-1 col-xs-1 col-md-1 center">
							      <input id="toggle-one" checked type="checkbox" data-on="Ativo" data-off="Inativo">
						     </div>							
						</div>
						<div class="contact-section">
					  		<div class="col-md-12 well well-sm "><h5>Dados Cadastrais </h5>
					  			<div class="col-lg-2 thumbnail"> <!-- ANO -->
						  			<p>
						  				E-Mail: <%--${doc.datePost}--%>
						  			</p>
								</div>
								<div class="col-lg-3 thumbnail"> <!-- Autor -->
						  			<p>Disciplina: 
<%-- 						  				<c:choose> --%>
<%-- 						  					<c:when test="${doc.authors!=null}"> --%>
<%-- 												<c:forEach var="authors" items="${doc.authors}"> --%>
<%-- 													${authors.name}, --%>
<%-- 												</c:forEach>  --%>
<%-- 											</c:when> --%>
<%-- 										<c:otherwise> --%>
<!-- 											Autores não cadastrados. -->
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
						  			</p>
							 		</div>
							    <div class="col-lg-4 thumbnail"> <!-- Palavra-chave -->
						  			<p>Último Post: 
<%-- 						  			<c:choose> --%>
<%-- 						  					<c:when test="${doc.keywords!=null}"> --%>
<%-- 												<c:forEach var="keywords" items="${doc.keywords}"> --%>
<%-- 													${keywords.word}, --%>
<%-- 												</c:forEach>  --%>
<%-- 											</c:when> --%>
<%-- 										<c:otherwise> --%>
<!-- 											Palavras chave não cadastradas. -->
<%-- 										</c:otherwise> --%>
<%-- 									</c:choose> --%>
						  			</p>
								</div>
<!-- 								<div class="col-sm-4 pull-right"> -->
						  			<div class="col-sm-2">	  	
							  			<div class="text-right ">
							  				<form method="post" action="<%-- ${pageContext.request.contextPath}--%>/SearchDocumentServlet"> 			  					
							  					<input type ="hidden" name="doc-id" value="<%-- ${doc.id}--%>" />
							  					<input type ="submit" class="btn btn-secundary" name="display-document" value="Visualizar" />
							  				</form>
							  			</div>
					  			</div>
							</div>
						</div>
					</div>
<%-- 				</c:forEach> --%>
<%-- 			</c:if> --%>
		</div>
		<script>
		  $(function() {
		    $('#toggle-one').bootstrapToggle();
		  })
		</script>
	</body>
</html>

		