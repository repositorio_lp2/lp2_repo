<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		<title>Contato</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<form class="form-horizontal"  method="post" action="${pageContext.request.contextPath}/ContactServlet">
						<c:if test="${successMessage != null}">
							<div class="alert alert-success alert-dismissable fade in">
								<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
								<strong>Sucesso:</strong> ${successMessage}
							</div>
						</c:if> 
						<c:if test="${contact!=null}">
							<c:if test="${failureMessages!=null}">
								<c:forEach var="m" items="${failureMessages}">
								<div class="alert alert-danger alert-dismissable fade in">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
									<strong>Erro:</strong> ${m}
								 </div>
								</c:forEach>
							</c:if>
						</c:if>
							<!-- Form Contact -->
							<div class="text-center">
								<h1>Contato</h1>
							</div>
							
							<!-- Name input-->
							<div class="form-group">
								<input id="name" name="name" value="${contact.name}" type="text" placeholder="Nome"
									class="form-control input-md" required>
							</div>
				
							<!-- Email input-->
							<div class="form-group">
								<input id="email" name="email" value="${contact.email}" type="email" placeholder="E-mail"
									class="form-control input-md" required>
							</div>
				
							<!-- About input-->
							<div class="form-group">
								<input id="subject" name="subject" value="${contact.subject}" type="text" placeholder="Assunto"
									class="form-control input-md" required>
							</div>
				
							<!-- Messenger input-->
							<div class="form-group">
								<textarea class="form-control"  id="message" name="message" 
								placeholder="Mensagem" maxlength="255" rows="7"><c:if test="${contact!=null}">${contact.messageContent}</c:if></textarea>
							</div>
							
							<!-- Button -->
							<div class="form-group text-center">
								<label class="col-md-4 control-label" for="submit"></label>
								<div class="col-md-4">
									<input id="submit" name="submit" type="submit" class="btn" value="Enviar">
								</div>
							</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>