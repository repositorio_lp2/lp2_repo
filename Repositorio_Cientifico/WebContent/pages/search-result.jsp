<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/search.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/font-awesome.min.css">
		<!--  -->
		<title>Resultado da Pesquisa</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<div class="container col-md-12">
			<div class="row">
				<fieldset>
					<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/SearchDocumentServlet">
						<div class="col-md-6 col-xs-10 conteudoSearchBar" align="left">
							<div class="form-group col-xs-9">
								<input type="text" class="form-control" name="search-text-input" placeholder="Texto da pesquisa" />
							</div>
							<div class="col-xs-3 form-group">
								<input id="submit" name="submit" type="submit" class="btn" value="Pesquisar">
							</div>
							<div class="row">
								<div class="form-group col-md-10 col-md-offset-0 center">
									<!-- Collapse -->
									<a href="#" id="advanced-search" class="btn btn-default" data-toggle="collapse" 
										data-target="#demo">Pesquisa Avançada 
										<span id="up-down" class="glyphicon glyphicon-chevron-down"></span></a>
								</div>
								<div class="col-md-10 col-md-offset-0">
									<!-- Collapse -->
									<div id="demo" class="collapse padding-inside">
										<div class="form-group well well-sm" style="padding:20px">
											<!-- Check box Title -->
											<div class="row">
												<div class="form-group col-xs-12 col-sm-12 col-md-4">
													<div class="form-inline">
															<span>Título</span>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
													<input id="title" name="title" type="text"
														placeholder="Título do Documento"
														class="form-control input-md">
												</div>
											</div>
											<!-- Check box Author -->
											<div class="row">
												<div class="form-group col-xs-12 col-sm-12 col-md-4">
													<div class="form-inline">
														<div class="checkbox">
															<label>Autor(es):</label>
														</div>
													</div>
												</div>
												<div
													class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
													<input id="author" name="author" type="text"
														placeholder="Autor 1, Autor 2, Autor 3, ..."
														class="form-control input-md" >
												</div>
											</div>
											<div class="row">
												<div class="form-group col-xs-12 col-sm-12 col-md-4">
													<!-- Check box Date -->
													<div class="form-inline">
														<div class="checkbox">
															<label>Data Início</label>
														</div>
													</div>
												</div>
												<div
													class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
													<input id="start-date" name="start-date" type="text" placeholder="Data"
														class="form-control input-md" >
												</div>
											</div>
											<div class="row">
												<div class="form-group col-xs-12 col-sm-12 col-md-4">
													<!-- Check box Date -->
													<div class="form-inline">
														<div class="checkbox">
															<label>Data Final</label>
														</div>
													</div>
												</div>
												<div
													class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
													<input id="final-date" name="final-date" type="text" placeholder="Data"
														class="form-control input-md" >
												</div>
											</div>
											<div class="row">
												<div class="form-group col-xs-12 col-sm-12 col-md-4">
													<!-- Check box Keywords -->
													<div class="form-inline">
														<div class="checkbox">
															<label>Palavra(s)-chave</label>
														</div>
													</div>
												</div>
												<div
													class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
													<input id="keyword" name="keyword" type="text"
														placeholder="Palavra 1, Palavra 2, Palavra 4, ..."
														class="form-control input-md" >
												</div>
											</div>
											<div class="row">
												<div class="form-group col-xs-12 col-sm-12 col-md-4">
													<!-- Check box Disciplina -->
													<div class="form-inline">
														<div class="checkbox">
															<label>Disciplina(s)</label>
														</div>
													</div>
												</div>
												<div
													class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
													<input id="discipline" name="discipline" type="text"
														placeholder="Disciplina 1, Disciplina 2, Disciplina 3, ..."
														class="form-control input-md" >
												</div>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</form>				
				</fieldset>		
			</div>
			<div class="form-group col-sm-6">		
				<h2>Resultado da Pesquisa</h2>
				<h4>___________________</h4></br>
			</div>
			<c:if test="${documents!=null}">
				<c:forEach var="doc" items="${documents}">
					<div class="container col-xs-10 col-md-12 thumbnail line-break">
						<div class="section-content">
							<h3 class="section-header">Título: ${doc.title}</h3>
						</div>
						<div class="contact-section">
					  		<div class="col-md-12 well well-sm "><h5>Descrição:</h5>
					  			<div class="form-group thumbnail ">
									<p>${doc.description}</p>
								</div>
								<h5>Resumo:</h5>
					  			<div class="form-group thumbnail ">
									<p>${doc.resume}</p>
								</div>
								<div class="col-lg-4 thumbnail"> <!-- Palavra-chave -->
						  			<p>Palavras-chave: 
						  			<c:choose>
						  					<c:when test="${doc.keywords!=null}">
												<c:forEach var="keywords" items="${doc.keywords}">
													${keywords.word},
												</c:forEach> 
											</c:when>
										<c:otherwise>
											Palavras chave não cadastradas.
										</c:otherwise>
									</c:choose>
						  			</p>
								</div>
								<div class="col-lg-3 thumbnail"> <!-- Autor -->
						  			<p>Autores: 
						  				<c:choose>
						  					<c:when test="${doc.authors!=null}">
												<c:forEach var="authors" items="${doc.authors}">
													${authors.name},
												</c:forEach> 
											</c:when>
										<c:otherwise>
											Autores não cadastrados.
										</c:otherwise>
									</c:choose>
						  			</p>
							 		</div>
							 	<div class="col-lg-2 thumbnail"> <!-- CATEGORIA -->
							 		<p>
							  			Categoria: ${doc.type.description}
							  		</p>
							  			
								</div>
							    <div class="col-lg-2 thumbnail"> <!-- ANO -->
						  			<p>
						  				Postado em: ${doc.datePost}
						  			</p>
								</div>
<!-- 								<div class="col-sm-4 pull-right"> -->
<!-- 						  			<div class="col-lg-9 text-center"> -->
<!-- 							  			<a href="https://www.facebook.com/Instituto-Federal-Catarinense-Campus-Rio-do-Sul-124651464906200/"> -->
<!-- 							  					<i id="#" class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a> -->
<!-- 			           					<a href="https://plus.google.com/"> -->
<!-- 												<i id="#" class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a> -->
<!-- 										<a href="https://br.linkedin.com/"> -->
<!-- 												<i id="#" class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a> -->
<!-- 			            				<a href="https://twitter.com/"> -->
<!-- 			            						<i id="#" class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a> -->
<!-- 									</div> -->
<!-- 									<div class="col-sm-2">	 -->
<!-- 							  			<div class="text-right form-group"> -->
<%-- 							  				<a href="${doc.path}" class="btn btn-default" download="${doc.path}">Download</a> <!-- LINK PARA DOWNLOAD DO DOCUMENTO --> --%>
<!-- 							  			</div> -->
<!-- 							  			<div class="text-right "> -->
<%-- 							  				<form method="post" action="${pageContext.request.contextPath}/SearchDocumentServlet"> 			  					 --%>
<%-- 							  					<input type ="hidden" name="doc-id" value="${doc.id}" /> --%>
<!-- 							  					<input type ="submit" class="btn btn-secundary" name="display-document" value="Visualizar" /> -->
<!-- 							  				</form> -->
<!-- 							  			</div> -->
<!-- 							  		</div> -->
<!-- 					  			</div> -->

								<div class="pull-right text-right form-group col-sm-1 col-lg-0 col-xs-1"> 
						  				<a href="${doc.path}" class="btn btn-default" download="${doc.path}">Download</a> <!-- LINK PARA DOWNLOAD DO DOCUMENTO -->
						  		</div>
								<div class="pull-right text-right col-sm-1 col-xs-1"> 
									<form method="post" action="${pageContext.request.contextPath}/SearchDocumentServlet"> 			  					
					  					<input type ="hidden" name="doc-id" value="${doc.id}" />
					  					<input type ="submit" class="btn btn-secundary" name="display-document" value="Visualizar" />
					  				</form>
				  				</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</body>
</html>