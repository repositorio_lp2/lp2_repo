<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/main-page-logo.css"></link>
		<!-- Captcha Google -->
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<title>Login</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<div class="container">
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<form class="form-horizontal" method="post" action="j_security_check">
						<fieldset>
							<!-- Image IFC -->
							<div class="form-group">
								<img src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/images/logoifc.png" 
										class="center-block" alt="" style="width: 50%" />
							</div>
							<!-- Success Messages -->
							<c:if test="${registerSuccess!=null || param.registerSuccess == 'true'}">
								<div class="alert alert-success">
									Usuário <strong>${param.registered}</strong> cadastrado com sucesso. Faça login para continuar. <br/>
									<strong>Usuários estão desabilitados até um administrador habilitá-los</strong>
								</div>
							</c:if>
							
							<!-- Error Messages -->
							<c:if test='${not empty param["retry"]}'>
								 <div class="alert alert-danger alert-dismissable fade in">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
									<strong>Erro:</strong> Usuário ou senha inválidos
								 </div>
							</c:if>
							<!-- Name input -->
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Nome do Usuário" name="j_username" />
							</div>
							
							<!-- Password input  -->
							<div class="form-group mbottom">
								<input type="password" class="form-control" placeholder="Senha" name="j_password"/>
							</div>
							
							<div class="row text-center">
								<div class="text-center col-md-2">
									<a href="${pageContext.request.contextPath}/pages/register.jsp" class="btn btn-default">Registrar</a>
								</div>
								<div class="text-center col-md-2">
									<input id="submit" name="submit" type="submit" class="btn" value="Login">
								</div>
							</div>			
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>