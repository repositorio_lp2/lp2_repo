<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		<title>Sobre</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		
		<div class="text-center">
			<h1>Sobre</h1>
		</div>
				
		<!-- Form About Text -->
		<div class="text-center">
			</br></br></br>
			<h4>Nós somos um grupo de pesquisa, chamado GDS - Grupo de Desenvolvimento de Software - situado no Instituto Federal Catarinense - Campus Rio do Sul, na Unidade Urbana.</h4>
		</div>
		<div class="text-center">
			</br></br></br>
			<h4>O objetivo desta plataforma é disponibilizar as produções científicas realizadas pelos dicentes e docentes do Instituto Federal Catarinense.</h4>
		</div>
	</body>
</html>