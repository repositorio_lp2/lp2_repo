<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<!-- 
		
			***********			INCOMPLETE!!!			***********	
		
		 -->	
	
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		
		<!-- Imports CSS and Scrip to Calendar -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/calendar.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap-datepicker.min.css" />
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap-datepicker3.min.css" />
		<script src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/bootstrap-datepicker.min.js"></script>
		<!-- Finish imports Calendar -->
	</head>
	<body>
	
	<div class="container">
			<div class="form-group">
				<div class="text-center">
					<div class="col-md-4 col-md-offset-4">
						<!-- Collapse -->
						<button type="button" class="btn" data-toggle="collapse"
							data-target="#demo">Pesquisa Avançada</button>
					</div>
				</div>
			</div>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<form class="form-horizontal ">
							<!-- Collapse -->
							<div id="demo" class="collapse">
	
	    <div class="form-group">
	        <div class="col-md-4 col-md-offset-2 date">
		        <div class="input-group input-append date col-md-4 col-md-offset-2" id="datePicker">
			        <input type="text" class="form-control input-md" name="date" id=""/>
				    <span class="input-group-addon add-on">
					<span class="glyphicon glyphicon-calendar"></span></span>
				</div>
			</div>
		</div>
		 <div class="form-group">
	        <div class="col-md-4 col-md-offset-2 date">
		        <div class="input-group input-append date col-md-4 col-md-offset-2" id="datePicker1">
			        <input type="text" class="form-control input-md" name="date" id=""/>
				    <span class="input-group-addon add-on">
					<span class="glyphicon glyphicon-calendar"></span></span>
				</div>
			</div>
		</div>
		</div></form></div></div></div>
	</body>
	<script>
		$(document).ready(function() {
			$('#datePicker').datepicker({
				format : 'mm/dd/yyyy'
			}).on('changeDate', function(e) {
				// Revalidate the date field
				$('#eventForm').formValidation('revalidateField', 'date');
			});

			$('#eventForm').formValidation({
				framework : 'bootstrap',
				icon : {
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				fields : {
					name : {
						validators : {
							notEmpty : {
								message : 'The name is required'
							}
						}
					},
					date : {
						validators : {
							notEmpty : {
								message : 'The date is required'
							},
							date : {
								format : 'MM/DD/YYYY',
								message : 'The date is not a valid'
							}
						}
					}
				}
			});
		});	
		
		$(document).ready(function() {
			$('#datePicker1').datepicker({
				format : 'mm/dd/yyyy'
			}).on('changeDate', function(e) {
				// Revalidate the date field
				$('#eventForm').formValidation('revalidateField', 'date');
			});

			$('#eventForm').formValidation({
				framework : 'bootstrap',
				icon : {
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				fields : {
					name : {
						validators : {
							notEmpty : {
								message : 'The name is required'
							}
						}
					},
					date : {
						validators : {
							notEmpty : {
								message : 'The date is required'
							},
							date : {
								format : 'MM/DD/YYYY',
								message : 'The date is not a valid'
							}
						}
					}
				}
			});
		});	
		
		/*
		//We then add validator rules for the selectedDate field, don't forget to set excluded: 
		//false to the field (because it's hidden so it will be ignored by default):
		$('#eventForm').formValidation({
		    framework: 'bootstrap',
		    icon: {
		        ...
		    },
		    fields: {
		        selectedDate: {
		            // The hidden input will not be ignored
		            excluded: false,
		            validators: {
		                notEmpty: {
		                    message: 'The date is required'
		                },
		                date: {
		                    format: 'MM/DD/YYYY',
		                    message: 'The date is not a valid'
		                }
		            }
		        }
		    }
		});
		
		//Finally, after choosing a date, you need to set the 
		//selected date to the hidden field, and revalidate it:
		('#embeddingDatePicker')
		    .datepicker({
		        format: 'mm/dd/yyyy'
		    })
		    .on('changeDate', function(e) {
		        // Set the value for the date input
		        $("#selectedDate").val($("#embeddingDatePicker").datepicker('getFormattedDate'));
		
		        // Revalidate it
		        $('#eventForm').formValidation('revalidateField', 'selectedDate');
		});
		*/
	</script>
</html>