<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/main-page.css"></link>
		
		<title>SAR - IFC Rio do Sul</title>
	</head>
	<body>
	<c:if test="${failureMessages!=null}">
		<c:forEach var="m" items="${failureMessages}">
			<div class="alert alert-danger alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>								    
					<strong>Erro:</strong> ${m}
			 </div>
		</c:forEach>
	</c:if>
	
		<!-- Import menu-hamburger -->
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/SearchDocumentServlet">
			<div class="container col-md-12">
				<div class="row">
					<!-- Image IFC -->
					<img
						src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/images/logoifc.png"
						class="form-groupImg center-block" alt="" style="width: 20%" />
					<!-- Search -->

					<div class="form-group"  >
						<div class="col-md-6 col-md-offset-3">

							<input type="text" name="search-text-input" class="form-control"
								placeholder="Texto da pesquisa" />
						</div>
						<div class="form-groupPA col">
							<input id="submit" name="submit" type="submit" class="btn" value="Pesquisar">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3 center ">
							<!-- Collapse -->
							<a href="#" id="advanced-search" class="btn btn-default" data-toggle="collapse" data-target="#demo">Pesquisa Avançada <span id="up-down" class="glyphicon glyphicon-chevron-down"></span></a>
						</div>
						
						<div class="col-md-6 col-md-offset-3 ">
							<!-- Collapse -->
							<div id="demo" class="collapse padding-inside">
								<div class="form-group well well-sm" style="padding:20px">
									<!-- Check box Title -->
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-4">
											<div class="form-inline">
													<span>Título</span>
											</div>
										</div>
										<div class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
											<input id="title" name="title" type="text"
												placeholder="Título do Documento"
												class="form-control input-md">
										</div>
									</div>
									<!-- Check box Author -->
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-4">
											<div class="form-inline">
												<div class="checkbox">
													<label>Autor(es):</label>
												</div>
											</div>
										</div>
										<div
											class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
											<input id="author" name="author" type="text"
												placeholder="Autor 1, Autor 2, Autor 3, ..."
												class="form-control input-md" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-4">
											<!-- Check box Date -->
											<div class="form-inline">
												<div class="checkbox">
													<label>Data Início:</label>
												</div>
											</div>
										</div>
										<div
											class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
											<input id="start-date" name="start-date" type="text" placeholder="Data"
												class="form-control input-md" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-4">
											<!-- Check box Date -->
											<div class="form-inline">
												<div class="checkbox">
													<label>Data Final:</label>
												</div>
											</div>
										</div>
										<div
											class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
											<input id="final-date" name="final-date" type="text" placeholder="Data"
												class="form-control input-md" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-4">
											<!-- Check box Keywords -->
											<div class="form-inline">
												<div class="checkbox">
													<label>Palavra(s)-chave:</label>
												</div>
											</div>
										</div>
										<div
											class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
											<input id="keyword" name="keyword" type="text"
												placeholder="Palavra 1, Palavra 2, Palavra 4, ..."
												class="form-control input-md" >
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 col-sm-12 col-md-4">
											<!-- Check box Disciplina -->
											<div class="form-inline">
												<div class="checkbox">
													<label>Disciplina(s):</label>
												</div>
											</div>
										</div>
										<div
											class="form-group col-xs-12 col-sm-12 col-md-8 col-md-offset-0">
											<input id="discipline" name="discipline" type="text"
												placeholder="Disciplina 1, Disciplina 2, Disciplina 3, ..."
												class="form-control input-md" >
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>