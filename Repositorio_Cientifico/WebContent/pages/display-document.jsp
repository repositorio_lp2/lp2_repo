<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Página de vizualização de documento</title>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/display-document.css"></link>
		<jsp:include page="head.jsp" flush="true"/>
	</head>
	<body>
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		<c:choose>
			<c:when test="${document!=null}">
		<div id="Iframe-Master-CC-and-Rs" class="set-margin set-padding set-border set-box-shadow center-block-horiz">
			<div class ="fullscreen-btn">
				<button class = "btn" onclick="full()">
					<span class="glyphicon glyphicon-fullscreen"> </span>
				</button>
			</div>
			<div class="responsive-wrapper 
		     responsive-wrapper-wxh-572x612"
		     style="-webkit-overflow-scrolling: touch; overflow: auto;">
			
		    <iframe allowfullscreen src="${document.path}"> 
			    <p style="font-size: 110%;">
			     	<em><strong>ERROR: </strong></em>Please update your browser to its most recent version and try again.
				</p>
			</iframe>
		  </div>
		</div>
		
		<div>
			<hr style="width:75%">
			<p class="wrap">Data Publicação: ${document.datePost}</p>
			<p class="wrap">Data Última Alteração: ${document.dateLastChange}</p>
			<p class="wrap">Enviado por: ${document.user.name}</p>
			
			<hr hr style="width:75%">
		</div> 
		</c:when>
		</c:choose> 
		<script type="text/javascript">
			function full(){
				var element = document.getElementsByTagName("iframe")[0];
				var requestMethod = element.requestFullScreen || 
				element.webkitRequestFullScreen || 
				element.mozRequestFullScreen || 
				element.msRequestFullscreen;
				requestMethod.call(element)
			}
		</script>
	
	<!-- https://codepen.io/alxfyv/pen/WxeLve?editors=1100 -->
	<!-- https://gordonlesti.com/bootstrap-responsive-embed-aspect-ratio/ -->
	</body>
</html>