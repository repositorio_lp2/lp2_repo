<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<jsp:include page="head.jsp" flush="true"/>
		<!-- Imports Links -->
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/search.css"></link>
		<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/font-awesome.min.css">
		
		<title>Gerência de Documento</title>
	</head>
	<body>
		<!-- Import menu-hamburger -->
		<jsp:include page="menu-hamburger.jsp" flush="true"/>
		<!--  -->
		<div class="container col-md-12">
			<div class="row">
				<fieldset>
					<form class="form-horizontal" method="post" action="${pageContext.request.contextPath}/SearchDocumentServlet">
						<div class="col-md-6 col-xs-10 conteudoSearchBar" align="left">
							<div class="form-group col-xs-9">
								<input type="text" class="form-control" placeholder="Texto da pesquisa" />
							</div>
							<div class="col-xs-3 form-group">
								<input id="submit" name="submit" type="submit" class="btn" value="Pesquisar">
							</div>
							<div class="row">
								<div class="form-group col-md-10 col-md-offset-0 center">
									<!-- Collapse -->
									<a href="#" id="advanced-search" class="btn btn-default" data-toggle="collapse" 
										data-target="#demo">Pesquisa Avançada 
										<span id="up-down" class="glyphicon glyphicon-chevron-down"></span></a>
								</div>
								<div class="col-md-10 col-md-offset-0">
									<!-- Collapse -->
									<div id="demo" class="collapse">
										<div class="form-group well well-sm">
											<!-- Check box Title -->
											<div class="row col-md-offset-0 col-xs-offset-0">
												<div class="form-group col-xs-12 col-sm-12 col-md-4 col-xs-4 col-sm-3">
													<div class="form-inline">
														<div class="checkbox">
															<input id="title-checkbox" name="title-checkbox" type="checkbox"> <label
																for="title-checkbox">Título</label>	
														</div>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-xs-8 col-sm-9">
													<input id="ln" name="ln" type="text"
														placeholder="Título do Documento"
														class="form-control input-md" required="">
												</div>
											</div>
											<!-- Check box Author -->
											<div class="row col-md-offset-0 col-xs-offset-0">
												<div class="form-group col-xs-12 col-sm-12 col-md-4 col-xs-4 col-sm-3">
													<div class="form-inline">
														<div class="checkbox">
															<input id="author-checkbox" name="author-checkbox" type="checkbox"> <label
																for="author-checkbox">Autor</label>	
														</div>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-xs-8 col-sm-9">
													<input id="ln" name="ln" type="text"
														placeholder="Autor 1, Autor 2, Autor 3, ..."
														class="form-control input-md" required="">
												</div>
											</div>
											<!-- Check box Date -->
											<div class="row col-md-offset-0 col-xs-offset-0">
												<div class="form-group col-xs-12 col-sm-12 col-md-4 col-xs-4 col-sm-3">
													<div class="form-inline">
													<div class="checkbox">
																<input id="start-date-checkbox" name="start-date-checkbox" type="checkbox"> <label
																for="start-date-checkbox">Data Início</label>	
														</div>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-xs-8 col-sm-9">
													<input id="ln" name="ln" type="text"
														placeholder="Date"
														class="form-control input-md" required="">
												</div>
											</div>
											<!-- Check box Date -->
											<div class="row col-md-offset-0 col-xs-offset-0">
												<div class="form-group col-xs-12 col-sm-12 col-md-4 col-xs-4 col-sm-3">
													<div class="form-inline">
														<div class="checkbox">
															<input id="final-date-checkbox" name="final-date-checkbox" type="checkbox"> <label
																for="final-date-checkbox">Data Final</label>	
														</div>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-xs-8 col-sm-9">
													<input id="ln" name="ln" type="text"
														placeholder="Date"
														class="form-control input-md" required="">
												</div>
											</div>
											<!-- Check box Keywords -->
											<div class="row col-md-offset-0 col-xs-offset-0">
													<div class="form-group col-xs-12 col-sm-12 col-md-4 col-xs-4 col-sm-3">
													<div class="form-inline">
														<div class="checkbox">
															<input id="keyword-checkbox" name="keyword-checkbox" type="checkbox"> <label
																for="keyword-checkbox">Palavra-chave</label>	
														</div>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-xs-8 col-sm-9">
													<input id="ln" name="ln" type="text"
														placeholder="Palavra 1, Palavra 2, Palavra 4, ..."
														class="form-control input-md" required="">
												</div>
											</div>
											<!-- Check box Disciplina -->
											<div class="row col-md-offset-0 col-xs-offset-0">
												<div class="form-group col-xs-12 col-sm-12 col-md-4 col-xs-4 col-sm-3">
													<div class="form-inline">
														<div class="checkbox">
															<input id="discipline-checkbox" name="discipline-checkbox" type="checkbox"> <label
																for="discipline-checkbox">Disciplina</label>	
														</div>
													</div>
												</div>
												<div class="form-group col-xs-12 col-sm-12 col-md-8 col-xs-8 col-sm-9">
												<input id="ln" name="ln" type="text"
														placeholder="Disciplina 1, Disciplina 2, Disciplina 3, ..."
														class="form-control input-md" required="">
												</div>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</form>				
				</fieldset>			
			</div>
			<div class="form-group col-sm-4">	
				<h2>Resulatdo da Pesquisa</h2>
				<h4>___________________</h4>
				<input id="#" name="#" type="submit" class="btn col-sm-6"
						value="Nova Submissão"> <!-- BUTTON FOR NEW DOCUMENT DELIVERY -->
			</div>
			<div class="container col-xs-10 col-md-12 thumbnail">
				<div class="section-content">
					<h3 class="section-header">Nome Documento 1</h3>
				</div>
				<div class="contact-section">
					<form>
				  		<div class="col-md-12 well well-sm">
				  			<div class="form-group thumbnail">
								<p>Descrição do Documento Descrição do Documento  Descrição do Documento
									 Descrição do Documento Descrição do Documento Descrição do Documento
									  Descrição do Documento Descrição do Documento Descrição do Documento
									   Descrição do Documento Descrição do Documento Descrição do Documento
									    Descrição do Documento Descrição do Documento Descrição do Documento
								     Descrição do Documento Descrição do Documento Descrição do Documento
									      Descrição do Documento Descrição do Documento Descrição do Documento
									       Descrição do Documento Descrição do Documento Descrição do Documento
									        Descrição do Documento Descrição do Documento Descrição do Documento
								</p>
							</div>
					  		<div class="col-lg-1 thumbnail"> <!-- YEAR -->
					  			<p>
					  				Ano 2133
					  			</p>
							</div>
							<div class="col-lg-3 thumbnail"> <!-- Author -->
					  			<p>
					  				Autor 1, Autor 2, Autor 3
					  			</p>
						 		</div>
						    <div class="col-lg-4 thumbnail"> <!-- Keywords -->
					  			<p>
					  				Catota, Jaguariuna, Giropop, Potatinho, Bolitus
					  			</p>
							</div>	
				  			<div class="text-right space-top">
				  				<div class="col-sm-3 text-right form-group">
							  		<a href="${doc.path}" download="${doc.path}">Download</a> <!-- LINK PARA DOWNLOAD DO DOCUMENTO -->
							  	</div>
							  	<div class="col-sm-3 text-right"> 			  					
							  		<a href="SearchDocumentServlet?op=display-document&id=${doc.id}">Visualizar</a> <!-- LINK PARA VISUALIZAR DOCUMENTO -->
							  	</div>
				  				<input id="#" name="#" type="submit" class="btn"
										value="Editar"> <!-- BUTTON FOR DOCUMENT EDIT -->		  					
				  				<input id="#" name="#" type="submit" class="btn btn-danger"
										value="Excluir"> <!-- BUTTON FOR CODUMENT DELETE -->
				  			</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>