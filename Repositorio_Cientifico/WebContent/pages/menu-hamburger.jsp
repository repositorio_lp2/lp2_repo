<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row menu-hamburger">
	<div class="col-sm-12">
	<div class="col-sm-11 m-b form-group">
		<div class="dropdown">
			<button class="btn dropdown-toggle" type="button"
				data-toggle="dropdown" data-submenu>
				<span class="glyphicon glyphicon-menu-hamburger"> </span>
			</button>

			<ul class="dropdown-menu">
				<li><a tabindex="0" href="${pageContext.request.contextPath}/pages/main-page.jsp">Home</a></li>
				<li><a tabindex="0" href="${pageContext.request.contextPath}/pages/about.jsp">Sobre</a></li>
				<li><a tabindex="0" href="${pageContext.request.contextPath}/pages/contact.jsp">Contato</a></li>
				<%
					if (request.getRemoteUser() != null && !request.getRemoteUser().isEmpty()) {
						request.setAttribute("userLogged", true);
					} else {
						request.setAttribute("userLogged", false);
					}
					
				%>
				<c:if test="${userLogged != null}">
					<c:if test="${!userLogged}">
						<li class="divider"></li>
						<li><a tabindex="0" href="${pageContext.request.contextPath}/pages/auth/user-dashboard.jsp">Login</a></li>
					</c:if>
				</c:if>
				<li class="divider"></li>
				<li><a tabindex="0" href="http://www.ifc-riodosul.edu.br/site/" target="_blank">Site Institucional</a></li>
				<li class="divider"></li>
				<li class="dropdown-submenu">
					<a >Redes Sociais</a>
					<ul class="dropdown-menu">
						<li><a tabindex="0" href="https://www.facebook.com/Instituto-Federal-Catarinense-Campus-Rio-do-Sul-124651464906200/" target="_blank">Facebook</a></li>
						<li><a tabindex="0" href="https://plus.google.com/" target="_blank">Google Plus</a></li>
						<li><a tabindex="0" href="https://br.linkedin.com/" target="_blank">LinkedIn</a></li>
						<li><a tabindex="0" href="https://twitter.com/" target="_blank">Twitter</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<c:if test="${userLogged != null}">
		<c:if test="${userLogged}">
			<div class="col-sm-1 m-b text-right">
				<div class="dropdown">
					<ul class="btn dropdown-toggle"
						data-toggle="dropdown" data-submenu>
						<span> <img src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/images/avatar.png"
							class="img-circle"/></span>
					</ul>
		
					<ul class="dropdown-menu">
						<li><a tabindex="0" href="${pageContext.request.contextPath}/pages/auth/user-dashboard.jsp">Home</a></li>
						<li><a tabindex="0" href="${pageContext.request.contextPath}/LogoutServlet">Logout</a></li>
					</ul>
				</div>
			</div>
		</c:if>
	</c:if>
	
	</div>
</div>

<script type="text/javascript">
	$('.dropdown-toggle').dropdown();
	$('[data-submenu]').submenupicker();
</script>