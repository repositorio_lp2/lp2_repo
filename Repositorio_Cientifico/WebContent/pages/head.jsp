<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- Imports permanents link -->
<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap-theme.min.css"></link>
<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/menu-hamburger.css"></link>
<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/bootstrap-submenu.min.css"></link>
<link rel="stylesheet" href="http://127.0.0.1:8080/Repositorio_Cientifico/resources/css/validation-fields.css"></link>

<!-- Imports permanents scripts -->
<script type="text/javascript" src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/jquery3-2-1.js"></script>
<script type="text/javascript" src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/advanced-search.js"></script>
<script type="text/javascript" src="http://127.0.0.1:8080/Repositorio_Cientifico/resources/js/bootstrap-submenu.min.js"></script>