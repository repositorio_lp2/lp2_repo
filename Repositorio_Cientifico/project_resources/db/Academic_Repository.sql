DROP DATABASE IF EXISTS Academic_Repository;
CREATE DATABASE IF NOT EXISTS Academic_Repository;

USE Academic_Repository;

-- -------------------------------------
-- Tabelas simples
-- -------------------------------------

CREATE TABLE IF NOT EXISTS user (
	id INT AUTO_INCREMENT,
    login VARCHAR(255),
    password VARCHAR(255),
    name VARCHAR(45),
    surname VARCHAR(45),
    email VARCHAR(45),
    active TINYINT,
    access_level TINYINT,
    deleted TINYINT DEFAULT 0,
    creation_date DATETIME,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS course (
	id INT AUTO_INCREMENT,
    description VARCHAR(45),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS author (
	id INT AUTO_INCREMENT,
    name VARCHAR(45),
    surname VARCHAR(45),
    email VARCHAR(250),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS discipline (
	id INT AUTO_INCREMENT,
    name VARCHAR(128),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS type (
	id INT AUTO_INCREMENT,
    description VARCHAR(45),
    PRIMARY KEY (id)
);

-- -------------------------------------
-- Tabelas Compostas
-- -------------------------------------

CREATE TABLE IF NOT EXISTS document (
	id INT AUTO_INCREMENT,
    user_id INT,
    type_id INT,
    path VARCHAR(128),
    title TEXT,
    description TEXT,
    date_post DATETIME,
    date_last_change DATETIME,
    abstract TEXT,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE
				ON UPDATE CASCADE,
    FOREIGN KEY (type_id) REFERENCES type(id) ON DELETE CASCADE
				ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS keyword (
	id INT AUTO_INCREMENT,
    document_id INT,
    word VARCHAR(45),
    PRIMARY KEY (id),
    FOREIGN KEY (document_id) REFERENCES document(id) ON DELETE CASCADE
			ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS role (
	id INT AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(45),
    description VARCHAR(45),
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES user(id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

-- -------------------------------------
-- Tabelas Dependentes
-- -------------------------------------

CREATE TABLE IF NOT EXISTS course_has_discipline (
	course_id INT,
    discipline_id INT,
    FOREIGN KEY (course_id) REFERENCES course(id)
			ON UPDATE CASCADE,
	FOREIGN KEY (discipline_id) REFERENCES discipline(id)
			ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS user_has_discipline (
	user_id INT,
    discipline_id INT,
    FOREIGN KEY (user_id) REFERENCES user(id)
			ON UPDATE CASCADE,
	FOREIGN KEY (discipline_id) REFERENCES discipline(id)
			ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS document_has_discipline (
	document_id INT,
    discipline_id INT,
    FOREIGN KEY (document_id) REFERENCES document(id) ON DELETE CASCADE
			ON UPDATE CASCADE,
	FOREIGN KEY (discipline_id) REFERENCES discipline(id) ON DELETE CASCADE
			ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS document_has_author (
	document_id INT AUTO_INCREMENT,
    author_id INT,
	FOREIGN KEY (document_id) REFERENCES document(id) ON DELETE CASCADE
			ON UPDATE CASCADE,
    FOREIGN KEY (author_id) REFERENCES author(id) ON DELETE CASCADE
			ON UPDATE CASCADE
);

-- -------------------------------------
-- INSERTS ESTÁTICOS
-- -------------------------------------


#testAdmin123
INSERT INTO `user` (`login`, `password`, `name`, `surname`, `email`, `active`, `access_level`, `creation_date`) 
			values (
					'admin',
                    '31fc4b3f58b3e036ef2acd2ebc4a9ac9',
                    'test',
                    'admin',
                    'test@admin.com',
                    1,
                    2,
                    '2017-11-22 23:00:00'
                    );


insert into role (`login`, `description`, `user_id`) 
		values ('admin', 'admin', 1);

insert into role (`login`, `description`, `user_id`) 
		values ('admin', 'instructor', 1);
        
insert into type (`description`)
			values ('TCC'),
					('MICT'),
                    ('FETEC'),
                    ('4C'),
                    ('OUTRO');